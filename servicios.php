<!DOCTYPE HTML>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Transvega - Servicios</title>
		<link rel="shortcut icon" type="image/x-icon" href="img/transvega-logo.png">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="css/icomoon.css">
		<link rel="stylesheet" type="text/css" href="css/principal.css">
	</head>
	<body>
		<div class="wrapper">
			<div class="container-fluid">
				<div id="nav-bar" class="navbar navbar-default navbar-fixed-top navbar-fijo">
					<div class="container">
						<div class="navbar-header page-scroll">
							<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#menu-principal">
								<span class="sr-only">Toggle Navegación</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="page-scroll" href="index.php"><img class="img-responsive logo" src="img/transvega-logo.png"></a>
						</div>
						
						<div class="collapse navbar-collapse" id="menu-principal">
							<ul class="nav navbar-nav">
								<li class="nombre-header">Transvega</li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
								<li><a class="activo" href="#">Servicios</a></li>
								<li><a href="rutas.php">Rutas</a></li>
								<li><a href="nosotros.php">Nosotros</a></li>
								<li><a href="contacto.php">Contáctenos</a></li>
								<li class="log"><a href="registro.php">Registro</a></li>
								<li class="log"><a href="#modalLogin" data-toggle="modal">Ingreso</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<br>
			<br>
			<br>
			<br>
			<br>
			<!---------------------------------------------------------------------------FIN MENU PRINCIPAL-------------------------------------------------------------------------------------->



		</div>		
		<?php
			include'includes/modals-cliente.php';
			include'includes/footer.php';
		?>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
	</body>
</html>