<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Transvega</title>
	<link rel="shortcut icon" type="image/x-icon" href="img/transvega-logo.png">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/principal.css">
	<link rel="stylesheet" type="text/css" href="css/principal-admin.css">

</head>
<body>
	<div class="wrapper">	
		<div class="container-fluid">
			<div id="nav-bar" class="navbar navbar-default navbar-fixed-top navbar-admin">
				<div class="container">
					<div class="navbar-header page-scroll">
						<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#menu-principal">
							<span class="sr-only">Toggle Navegación</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="page-scroll" href="index.php"><img class="img-responsive logo" src="img/transvega-logo.png"></a>

					</div>
					<div class="collapse navbar-collapse" id="menu-principal">
						<ul class="nav navbar-nav">
							<li class="nombre-header">Transvega</li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li><a href="admin-envios.php">Envíos</a></li>
							<li><a href="#">Clientes</a></li>
							<li><a href="#">Empleados</a></li>
							<li><a class="activo" href="#">Vehículos</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<br>
		<br>
		<br>
		<br>
		<br>
		<div class="container modulo-titulo"><h1 class="text-center">Gestión Vehículos</h1></div>
		<br>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-lg-3">
					<ul class="nav nav-tabs nav-stacked">
						<li class="active"><a href="#menu1" data-toggle="tab"><i class="fa fa-truck"></i>&nbsp;Camiones</a></li>
						<li><a href="#menu2" data-toggle="tab"><i class="fa fa-building"></i>&nbsp;Talleres</a></li>
						<li>
							<a href="#menu3"data-toggle="tab">
								<i class="fa fa-wrench"></i>
								&nbsp;Mantenimiento
							</a>
					     </li>
					</ul>
				</div>
				
				<div class="col-xs-12 col-lg-9">
					<div class="tab-content">
						<div class="tab-pane active" id="menu1"><!Camiones>
							<div class="panel panel-default"> 
							  	<div class="panel-heading">
							    	<h4 class="panel-title panel-principal"><i class="fa fa-truck"></i>&nbsp;Camiones</h4>
							  	</div>
								<div class="panel-body">
							    	<div class="panel-group"><!Acciones de Camiones>

										<div class="panel panel-info"><!Consultar Camiones>
											<div class="panel-heading">
												<h4 class="panel-title">
													<a data-toggle="collapse" href="#consultar-camiones" aria-expanded="true">
									         		<span class="fa fa-search" aria-hidden="true"></span> Consultar
									        		</a>	
												</h4>
											</div>

											<div id="consultar-camiones" class="panel-collapse collapse in">
												<div class="panel-body">
													<form id="buscarCamion">
														<table id="buscarCamionesT" class="table table-striped table-bordered" cellspacing="0" width="100%">
															
														    <thead>
														        <tr>
														        	<th><span class="fa fa-plus" aria-hidden="true"></span></th>
														            <th>Matrícula</th>
														            <th>Modelo</th>
														            <th>Estado</th>
														            <th>Chofer</th>
														            <th>Ult. Mantenimiento</th> 
														        </tr>
														    </thead>
														 
														    <tfoot>
														        <tr>
														        	<th><span class="fa fa-plus" aria-hidden="true"></span></th>
														            <th>Matrícula</th>
														            <th>Modelo</th>
														            <th>Estado</th>
														            <th>Chofer</th>
														            <th>Ult. Mantenimiento</th>
														        </tr>
														    </tfoot>
														 
														    <tbody>
														        <tr>
														        	<td><a href="#CH5400BA" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
														            <td>CH5400BA</td>
														            <td>F-350</td>
														            <td><span class="badge badge-success">Disponible</span></td>
														            <td>-</td>
														            <td>05/01/2015</td>
														        </tr>
														        <tr>
														        	<td><a href="#DH5403BA" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
														            <td>DH5403BA</td>
														            <td>NPR</td>
														            <td><span class="badge badge-warning">Activo</span></td>
														            <td>Argenis Rodríguez</td>
														            <td>27/12/2014</td>
														        </tr>
														        <tr>
														        	<td><a href="#TC7203AB" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
														            <td>TC7203AB</td>
														            <td>NPR</td>
														            <td><span class="badge badge-success">Disponible</span></td>
														            <td>-</td>
														            <td>10/01/2015</td>
														        </tr>
														        <tr>
														        	<td><a href="#F54F3C3" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
														            <td>F54F3C3</td>
														            <td>F-750</td>
														            <td><span class="badge badge-warning">Activo</span></td>
														            <td>José Contreras</td>
														            <td>20/12/2014</td>
														        </tr>
														        <tr>
														        	<td><a href="#F324C1B9" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
														            <td>F324C1B9</td>
														            <td>NPR</td>
														            <td><span class="badge badge-danger">Mantenimiento</span></td>
														            <td>-</td>
														            <td>20/01/2015</td>
														        </tr>
														    </tbody>
														    
														</table>
									        		</form>
												</div>
											</div>		
										</div> <!Fin Consultar Camiones>

										<div class="panel panel-success"><!Agregar Camión>
											<div class="panel-heading">
												<h4 class="panel-title">
													<a data-toggle="collapse" href="#agregar" aria-expanded="false">
									         			<span class="fa fa-plus-square" aria-hidden="true"></span> Agregar
									        		</a>	
												</h4>
											</div>

											<div id="agregar" class="panel-collapse collapse">
												<div class="panel-body">
													<form id="agregar_vehiculo" class="form-horizontal">
														<div class="form-group">
													    	<label for="matricula" class="col-xs-12 col-sm-4 control-label">Matrícula</label>
													    	<div class="col-xs-12 col-sm-8">
													    		<input type="text" name="matricula" class="form-control" id="matricula" placeholder="Ejm: TC7203AB" required autofocus>
															</div>
														</div>
														<div class="form-group">
													    	<label for="carga" class="col-xs-12 col-sm-4 control-label">Capacidad de Carga</label>
													    	<div class="col-xs-12 col-sm-8">
													    		<div class="input-group">
													    			<div class="input-group-addon">Kg.</div>
													    			<input type="text" name="carga" class="form-control" id="carga" placeholder="Ejm: 5000" required>
													    		</div>
													    	</div>
														</div>
														<div class="form-group">
													    	<label for="observacion" class="col-xs-12 col-sm-4 control-label">Observaciones</label>
													    	<div class="col-xs-12 col-sm-8">
													    		<textarea name="observacion" id="observacion" class="form-control" rows="3"  placeholder="Características del Vehículo"></textarea>
													    	</div>
														</div>
														<div class="form-group">
															<label for="color" class="col-xs-12 col-sm-4 control-label">Color</label>
															<div class="col-xs-12 col-sm-2">
																<input type="color" name="color" class="form-control" id="color" required>
															</div>
															<label for="year" class="col-xs-12 col-sm-2 control-label">Año</label>
															<div class="col-xs-12 col-sm-2">
																<input type="number" name="year" min="1980" max="2015" class="form-control" step="1" value="2015">
															</div>
														</div>
														<div class="form-group">
															<label for="marca" class="col-xs-12 col-sm-4 control-label">Marca</label>
															<div class="col-xs-12 col-sm-8">
																<select class="form-control" name="marca" id="marca">
																	<option>-Seleccione-</option>
																	<option>Acura</option>
																	<option>Chevrolet</option>
																	<option>Ford</option>
																	<option>Horda</option>
																	<option>Toyota</option>
																</select>
															</div>
														</div>
														<div class="form-group">
															<label for="modelo" class="col-xs-12 col-sm-4 control-label">Modelo</label>
															<div class="col-xs-12 col-sm-8">
																<select class="form-control" name="modelo" id="modelo">
																	<option>-Seleccione-</option>
																	<option>F-350</option>
																	<option>F-750</option>
																	<option>NPR</option>
																</select>
															</div>
														</div>
														<div class="form-group">
															<div class="col-xs-12 col-sm-6 col-sm-push-6 col-md-4 col-md-push-8 col-lg-4 col-lg-push-8">
																<button type="button" id="BotonAgregar" data-loading-text="Agregando..." class="btn btn-primary btn-block btn-lg" autocomplete="off">
									  								Agregar
																</button>
															</div>
														</div>
									        		</form>
												</div>
											</div>		
										</div> <!Fin Agregar Camiones>
										
										<div class="panel panel-warning"><!Modificar Camión>
											<div class="panel-heading">
												<h4 class="panel-title">
													<a data-toggle="collapse" href="#modificar_camion" aria-expanded="false">
									         			<span class="fa fa-pencil" aria-hidden="true"></span> Modificar
									        		</a>	
												</h4>
											</div>

											<div id="modificar_camion" class="panel-collapse collapse">
												<div class="panel-body">
													<form id="modificarCamion">
														<table id="modificarCamionesT" class="table table-striped table-bordered" cellspacing="0" width="100%">
														    <thead>
														        <tr>
														        	<th><span class="fa fa-plus" aria-hidden="true"></span></th>
														            <th>Matrícula</th>
														            <th>Modelo</th>
														            <th>Marca</th>
														            <th>Año</th>
														            <th>Capacidad</th> 
														        </tr>
														    </thead>
														 
														    <tfoot>
														        <tr>
														        	<th><span class="fa fa-plus" aria-hidden="true"></span></th>
														            <th>Matrícula</th>
														            <th>Modelo</th>
														            <th>Marca</th>
														            <th>Año</th>
														            <th>Capacidad</th>
														        </tr>
														    </tfoot>
														    <tbody>
														        <tr>
														        	<td><a href="#editar" data-toggle="modal"><i class="fa fa-pencil"></i></a></td>
														            <td>CH5400BA</td>
														            <td>F-350</td>
														            <td>Ford</td>
														            <td>2010</td>
														            <td>3328</td>
														        </tr>
														        <tr>
														        	<td><a href="#editar" data-toggle="modal"><i class="fa fa-pencil"></i></a></td>
														            <td>DH5403BA</td>
														            <td>NPR</td>
														            <td>Chevrolet</td>
														            <td>2005</td>
														            <td>4835</td>
														        </tr>
														        <tr>
														        	<td><a href="#editar" data-toggle="modal"><i class="fa fa-pencil"></i></a></td>
														            <td>TC7203AB</td>
														            <td>NPR</td>
														            <td>Chevrolet</td>
														            <td>2007</td>
														            <td>4835</td>
														        </tr>
														        <tr>
														        	<td><a href="#editar" data-toggle="modal"><i class="fa fa-pencil"></i></a></td>
														            <td>F54F3C3</td>
														            <td>F-750</td>
														            <td>Ford</td>
														            <td>2001</td>
														            <td>9300</td>
														        </tr>
														        <tr>
														        	<td><a href="#editar" data-toggle="modal"><i class="fa fa-pencil"></i></a></td>
														            <td>F324C1B9</td>
														            <td>NPR</td>
														            <td>Chevrolet</td>
														            <td>2006</td>
														            <td>4835</td>
														        </tr>
														    </tbody>
														    
														</table>
									        		</form>
												</div>
											</div>		
										</div> <!Fin Modificar Camiones>

										<div class="panel panel-danger"><!Eliminar Camión>
											<div class="panel-heading">
												<h4 class="panel-title">
													<a data-toggle="collapse" href="#eliminar_camion" aria-expanded="false">
									         			<span class="fa fa-trash" aria-hidden="true"></span> Eliminar
									        		</a>	
												</h4>
											</div>

											<div id="eliminar_camion" class="panel-collapse collapse">
												<div class="panel-body">
													<form id="eliminar_vehiculo" class="form-horizontal">
														<table id="eliminarCamionesT" class="table table-striped table-bordered" cellspacing="0" width="100%">
															
														    <thead>
														        <tr>
														        	<th><span class="fa fa-plus" aria-hidden="true"></span></th>
														            <th>Matrícula</th>
														            <th>Modelo</th>
														            <th>Marca</th>
														            <th>Año</th>
														            <th>Capacidad</th> 
														        </tr>
														    </thead>
														 
														    <tfoot>
														        <tr>
														        	<th><span class="fa fa-plus" aria-hidden="true"></span></th>
														            <th>Matrícula</th>
														            <th>Modelo</th>
														            <th>Marca</th>
														            <th>Año</th>
														            <th>Capacidad</th>
														        </tr>
														    </tfoot>
														    <tbody>
														        <tr>
														        	<td><label><input type="checkbox"></label></td>
														            <td>CH5400BA</td>
														            <td>F-350</td>
														            <td>Ford</td>
														            <td>2010</td>
														            <td>3328</td>
														        </tr>
														        <tr>
														        	<td><label><input type="checkbox"></label></td>
														            <td>DH5403BA</td>
														            <td>NPR</td>
														            <td>Chevrolet</td>
														            <td>2005</td>
														            <td>4835</td>
														        </tr>
														        <tr>
														        	<td><label><input type="checkbox"></label></td>
														            <td>TC7203AB</td>
														            <td>NPR</td>
														            <td>Chevrolet</td>
														            <td>2007</td>
														            <td>4835</td>
														        </tr>
														        <tr>
														        	<td><label><input type="checkbox"></label></td>
														            <td>F54F3C3</td>
														            <td>F-750</td>
														            <td>Ford</td>
														            <td>2001</td>
														            <td>9300</td>
														        </tr>
														        <tr>
														        	<td><label><input type="checkbox"></label></td>
														            <td>F324C1B9</td>
														            <td>NPR</td>
														            <td>Chevrolet</td>
														            <td>2006</td>
														            <td>4835</td>
														        </tr>
														    </tbody>
														    
														</table>
														<br>
														<div class="col-xs-12 col-sm-6 col-sm-push-6 col-md-4 col-md-push-8 col-lg-4 col-lg-push-8">
															<button type="button" id="BotonEliminar" data-loading-text="Eliminando..." class="btn btn-danger btn-block btn-lg" autocomplete="off" onclick="eliminarVehiculo()">
								  								Eliminar
															</button>
														</div>
													
									        		</form>
												</div>
											</div>		
										</div> <!Fin Eliminar Camiones>

									</div> <!Fin Acciones de Camiones>
							  	</div>
							</div> 
						</div> <!Fin Camiones>
						<div class="tab-pane" id="menu2">
							<div class="panel panel-default">
							  	<div class="panel-heading">
							    	<h3 class="panel-title"><i class="fa fa-building"></i>&nbsp;Talleres</h3>
							  	</div>
								<div class="panel-body">
							    	
							  	</div>
							</div>
						</div>
						<div class="tab-pane" id="menu3">
							<div class="panel panel-default">
							  	<div class="panel-heading">
							    	<h3 class="panel-title"><i class="fa fa-wrench"></i>&nbsp;Mantenimiento</h3>
							  	</div>
								<div class="panel-body">
							    	Panel content
							  	</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!*************************************************MODALS*********************************************************>

	<div class="modal fade" id="CH5400BA" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  	<div class="modal-dialog">
		    <div class="modal-content">
		     	<div class="modal-header modal-info">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        	<h4 class="modal-title admin-titulo" id="myModalLabel">Vehículo</h4>
		      	</div>
		      	<div class="modal-body">
	      			<div class="img-contenedor img-center">
						<div class="row">
							<img src="img/vehiculo/F350.jpg"  alt="Camión Ford F-350" class="img-responsive img-thumbnail img-admin">
						</div>
					</div>
					<br>
					<form class="login" action="">
						<div class="form-group has-feedback">
							<h2>
								<strong>Matrícula: </strong> CH5400BA 
							</h2>	
						</div>
						<div class="form-group has-feedback">
							<h3>
								<strong>Modelo: </strong> F-350
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<strong>Marca: </strong> Ford
							</h3>
						</div>
						<div class="form-group has-feedback">
								<h3><strong>Capacidad Carga (kg): </strong> 3328</h3>
						</div>
						<div class="form-group has-feedback">
								<h4><strong>Observaciones: </strong></h4>
								<p class="text-justify">Motor 6.2L 16V SFI SOHC V8 con un desplazamiento de 5408 cc que genera una potencia de 385 caballos de fuerza. Sistema de Combustible de Inyección electrónica secuencial con un Tanque de Combustible para Gasolina sin plomo de 151 lts de capacidad </p>
								
						</div>
						<div class="form-group has-feedback">
								<h4><strong>Estado: </strong> <span class="badge badge-success">Disponible</span></h4>
						</div>
						<div class="form-group has-feedback">
								<h4><strong>Ubicación: </strong></h4>
								<p>Caracas - Base</p>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-sm-push-6 col-md-6 col-md-push-6 col-lg-4 col-lg-push-8">
								<button type="button" href="" class="btn btn-lg btn-info btn-block">Registro</button>
							</div>
						</div>
					</form>
			    </div>
		    </div>
	  	</div>
	</div>
	
	<div class="modal fade" id="DH5403BA" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  	<div class="modal-dialog">
		    <div class="modal-content">
		     	<div class="modal-header modal-info">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        	<h4 class="modal-title admin-titulo" id="myModalLabel">Vehículo</h4>
		      	</div>
		      	<div class="modal-body">
	      			<div class="img-contenedor img-center">
						<div class="row">
							<img src="img/vehiculo/NPR.jpg"  alt="Camión Chevrolet NPR" class="img-responsive img-thumbnail img-admin">
						</div>
					</div>
					<br>
					<form class="login" action="">
						<div class="form-group has-feedback">
							<h2>
								<strong>Matrícula: </strong> DH5403BA 
							</h2>	
						</div>
						<div class="form-group has-feedback">
							<h3>
								<strong>Modelo: </strong> NPR
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<strong>Marca: </strong> Chevrolet
							</h3>
						</div>
						<div class="form-group has-feedback">
								<h3><strong>Capacidad Carga (kg): </strong> 4835</h3>
						</div>
						<div class="form-group has-feedback">
								<h4><strong>Observaciones: </strong></h4>
								<p class="text-justify">Motor Marca Isuzu 4HK1-TCN Tipo Turbo Intercooler 4 Cilindros en línea, Transmisión del Tipo Hidráulica</p>
								
						</div>
						<div class="form-group has-feedback">
								<h4><strong>Estado: </strong> <span class="badge badge-success">Disponible</span></h4>
						</div>
						<div class="form-group has-feedback">
								<h4><strong>Ubicación: </strong></h4>
								<p id="ubicacion"></p>
						</div>
						<div id="map-canvas" class="map-canvas form-group"></div>
						<div class="row form-group">
							<div class="col-xs-12 col-sm-6 col-sm-push-6 col-md-6 col-md-push-6 col-lg-4 col-lg-push-8">
								<button type="button" href="" class="btn btn-lg btn-info btn-block">Registro</button>
							</div>
						</div>
					</form>
			    </div>
		    </div>
	  	</div>
	</div>
	
	<div class="modal fade" id="TC7203AB" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  	<div class="modal-dialog">
		    <div class="modal-content">
		     	<div class="modal-header modal-info">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        	<h4 class="modal-title admin-titulo" id="myModalLabel">Vehículo</h4>
		      	</div>
		      	<div class="modal-body">
	      			<div class="img-contenedor img-center">
						<div class="row">
							<img src="img/vehiculo/NPR.jpg"  alt="Camión Chevrolet NPR" class="img-responsive img-thumbnail img-admin">
						</div>
					</div>
					<br>
					<form class="login" action="">
						<div class="form-group has-feedback">
							<h2>
								<strong>Matrícula: </strong> TC7203AB 
							</h2>	
						</div>
						<div class="form-group has-feedback">
							<h3>
								<strong>Modelo: </strong> NPR
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<strong>Marca: </strong> Chevrolet
							</h3>
						</div>
						<div class="form-group has-feedback">
								<h3><strong>Capacidad Carga (kg): </strong> 4835</h3>
						</div>
						<div class="form-group has-feedback">
								<h4><strong>Observaciones: </strong></h4>
								<p class="text-justify">Motor Marca Isuzu 4HK1-TCN Tipo Turbo Intercooler 4 Cilindros en línea, Transmisión del Tipo Hidráulica</p>
								
						</div>
						<div class="form-group has-feedback">
								<h4><strong>Estado: </strong> <span class="badge badge-warning">Activo</span></h4>
						</div>
						<div class="form-group has-feedback">
								<h4><strong>Ubicación: </strong></h4>
								<p>Caracas - Base</p>
						</div>
						<div class="row form-group">
							<div class="col-xs-12 col-sm-6 col-sm-push-6 col-md-6 col-md-push-6 col-lg-4 col-lg-push-8">
								<button type="button" href="" class="btn btn-lg btn-info btn-block">Registro</button>
							</div>
						</div>
					</form>
			    </div>
		    </div>
	  	</div>
	</div>

	<div class="modal fade" id="F54F3C3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  	<div class="modal-dialog">
		    <div class="modal-content">
		     	<div class="modal-header modal-info">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        	<h4 class="modal-title admin-titulo" id="myModalLabel">Vehículo</h4>
		      	</div>
		      	<div class="modal-body">
	      			<div class="img-contenedor img-center">
						<div class="row">
							<img src="img/vehiculo/F750.jpg"  alt="Camión Ford F750" class="img-responsive img-thumbnail img-admin">
						</div>
					</div>
					<br>
					<form class="login" action="">
						<div class="form-group has-feedback">
							<h2>
								<strong>Matrícula: </strong> F54F3C3 
							</h2>	
						</div>
						<div class="form-group has-feedback">
							<h3>
								<strong>Modelo: </strong> F750
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<strong>Marca: </strong> Ford
							</h3>
						</div>
						<div class="form-group has-feedback">
								<h3><strong>Capacidad Carga (kg): </strong> 9300</h3>
						</div>
						<div class="form-group has-feedback">
								<h4><strong>Observaciones: </strong></h4>
								<p class="text-justify">Motor Cummins® ISB de 6.7L, Transmisión automática de 6 velocidades con sobremarcha 2500RDS/WR Allison, Frenos hidraúlicos: Sistema de Frenos de Disco Quadraulic™ en las 4 Ruedas</p>
						</div>
						<div class="form-group has-feedback">
								<h4><strong>Estado: </strong> <span class="badge badge-success">Disponible</span></h4>
						</div>
						<div class="form-group has-feedback">
								<h4><strong>Ubicación: </strong></h4>
								<p id="ubicacion2"></p>
						</div>
						<div id="map-canvas2" class="map-canvas form-group"></div>
						<div class="row form-group">
							<div class="col-xs-12 col-sm-6 col-sm-push-6 col-md-6 col-md-push-6 col-lg-4 col-lg-push-8">
								<button type="button" href="" class="btn btn-lg btn-info btn-block">Registro</button>
							</div>
						</div>
					</form>
			    </div>
		    </div>
	  	</div>
	</div>

	<div class="modal fade" id="editar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  	<div class="modal-dialog">
		    <div class="modal-content">
		     	<div class="modal-header modal-info">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        	<h4 class="modal-title admin-titulo" id="myModalLabel"><span class="fa fa-pencil" aria-hidden="true"></span>&nbsp; Editar Vehículo</h4>
		      	</div>
		      	<div class="modal-body">
					<form id="agregar_vehiculo" class="form-horizontal">
						<div class="form-group">
					    	<label for="matricula" class="col-xs-12 col-sm-4 control-label">Matrícula</label>
					    	<div class="col-xs-12 col-sm-8">
					    		<input type="text" name="matricula" class="form-control" id="matricula" placeholder="Ejm: TC7203AB" required autofocus value="F54F3C3">
							</div>
						</div>
						<div class="form-group">
					    	<label for="carga" class="col-xs-12 col-sm-4 control-label">Capacidad de Carga</label>
					    	<div class="col-xs-12 col-sm-8">
					    		<div class="input-group">
					    			<div class="input-group-addon">Kg.</div>
					    			<input type="text" name="carga" class="form-control" id="carga" placeholder="Ejm: 5000" required value="9300">
					    		</div>
					    	</div>
						</div>
						<div class="form-group">
					    	<label for="observacion" class="col-xs-12 col-sm-4 control-label">Observaciones</label>
					    	<div class="col-xs-12 col-sm-8">
					    		<textarea name="observacion" id="observacion" class="form-control" rows="3"  placeholder="Características del Vehículo">Motor Cummins® ISB de 6.7L, Transmisión automática de 6 velocidades con sobremarcha 2500RDS/WR Allison, Frenos hidraúlicos: Sistema de Frenos de Disco Quadraulic™ en las 4 Ruedas</textarea>
					    	</div>
						</div>
						<div class="form-group">
							<label for="marca" class="col-xs-12 col-sm-4 control-label">Marca</label>
							<div class="col-xs-12 col-sm-8">
								<select class="form-control" name="marca" id="marca">
									<option>-Seleccione-</option>
									<option>Acura</option>
									<option>Chevrolet</option>
									<option selected>Ford</option>
									<option>Horda</option>
									<option>Toyota</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="modelo" class="col-xs-12 col-sm-4 control-label">Modelo</label>
							<div class="col-xs-12 col-sm-8">
								<select class="form-control" name="modelo" id="modelo">
									<option>-Seleccione-</option>
									<option>F-350</option>
									<option selected>F-750</option>
									<option>NPR</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-sm-6 col-sm-push-6 col-md-4 col-md-push-8 col-lg-4 col-lg-push-8">
								<button type="button" id="BotonGuardar" data-loading-text="Agregando..." class="btn btn-warning btn-block btn-lg" autocomplete="off">
	  								Guardar
								</button>
							</div>
						</div>
	        		</form>
			    </div>
		    </div>
	  	</div>
	</div>

	<!*************************************************FOOTER*********************************************************>
	<footer>
		<div class="container-fluid pie-pagina">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-8 col-lg-8">
						<h4>Transvega C. A. - Rif: J-404377344</h4>
						<p class="small">Telfs: (0212) 3152932, (0416) 6108818, (0426) 5120634</p>
						<p class="small"> Tercera Avenida con Segunda Transversal, Urbanización Los Palos Grandes, Edificio Augustus I Piso 5 Oficina 502, Municipio Chacao, Distrito Capital.</p>
					</div>
					<div class="col-xs-12 col-sm-4 col-lg-4">
						<br>
						<h3 class="text-center">
							<a href="https://www.facebook.com/profile.php?id=100006442804682&fref=ts" target="_blank" class="fa fa-facebook fa-2x social"></a>
							<a href="http://instagram.com/Transvega/" target="_blank" class="fa fa-instagram fa-2x social"></a>
							<a href="https://twitter.com/TransvegaCA" target="_blank" class="fa fa-twitter fa-2x social"></a>
						<h3>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="js/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="js/header.js"></script>
	<script type="text/javascript" src="js/tabla-idioma.js"></script>
	<script type="text/javascript" src="js/admin.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
	<script>
			$('<div id="buscarCamionesDIV" class="table-responsive"></div>').insertAfter( "#buscarCamionesT" );
			$('#buscarCamionesT').appendTo('#buscarCamionesDIV');

			$('<div id="modificarCamionesDIV" class="table-responsive"></div>').insertAfter( "#modificarCamionesT" );
			$('#modificarCamionesT').appendTo('#modificarCamionesDIV');

			$('<div id="eliminarCamionesDIV" class="table-responsive"></div>').insertAfter( "#eliminarCamionesT" );
			$('#eliminarCamionesT').appendTo('#eliminarCamionesDIV');

		});

		$(function () 
		{
  			$('[data-toggle="popover"]').popover()
		})



		var directionsDisplay;
		var directionsService;
		var map;
		var latitud_ubicacion;
		var longitud_ubicacion;
		var latitud_destino;
		var longitud_destino;
		var ubicacion;
		var destino;
		var xlat;
		var ylon;

		function calcRoute() 
		{
		  var selectedMode = "DRIVING";
		  var request = 
		  {
		      origin: ubicacion,
		      destination: destino,
		      travelMode: google.maps.TravelMode[selectedMode]
		  };
		  directionsService.route(request, function(response, status) 
		  {
		    if (status == google.maps.DirectionsStatus.OK) 
		    {
		      directionsDisplay.setDirections(response);
		    }
		  });
		}

		$("#DH5403BA").on('shown.bs.modal',function () 
		{
			directionsService = new google.maps.DirectionsService();
			latitud_ubicacion = 10.279817;
			longitud_ubicacion = -67.700043;
			latitud_destino = 10.629577;
			longitud_destino = -71.640698;
			ubicacion = new google.maps.LatLng(latitud_ubicacion, longitud_ubicacion);
			destino = new google.maps.LatLng(latitud_destino, longitud_destino);
			xlat = latitud_ubicacion.toString();
			ylon = longitud_ubicacion.toString();

			var peticion = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+xlat+","+ylon+"&sensor=true_or_false";
			
			$.getJSON(peticion, {format: "json"}, function(data) 
			{ 
				$("#ubicacion").html(data.results[0].formatted_address);
			});


			directionsDisplay = new google.maps.DirectionsRenderer();
		    var mapOptions = {
		        center: ubicacion,
		        zoom: 18,
		        draggable: true,
		        scrollwheel: true,
		        mapTypeId: google.maps.MapTypeId.ROADMAP
		    };
		    map = new google.maps.Map(document.getElementById("map-canvas"),mapOptions);
		    calcRoute();
		    directionsDisplay.setMap(map);
		    google.maps.event.trigger(map, "resize");

		});

		$("#F54F3C3").on('shown.bs.modal',function () 
		{
			directionsService = new google.maps.DirectionsService();
			latitud_ubicacion = 10.488435;
			longitud_ubicacion = -66.844095;
			latitud_destino = 10.473525;
			longitud_destino = -64.164748;
			ubicacion = new google.maps.LatLng(latitud_ubicacion, longitud_ubicacion);
			destino = new google.maps.LatLng(latitud_destino, longitud_destino);
			xlat = latitud_ubicacion.toString();
			ylon = longitud_ubicacion.toString();

			var peticion = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+xlat+","+ylon+"&sensor=true_or_false";
			
			$.getJSON(peticion, {format: "json"}, function(data) 
			{ 
				$("#ubicacion2").html(data.results[0].formatted_address);
			});


			directionsDisplay = new google.maps.DirectionsRenderer();
		    var mapOptions = {
		        center: ubicacion,
		        zoom: 18,
		        draggable: true,
		        scrollwheel: true,
		        mapTypeId: google.maps.MapTypeId.ROADMAP
		    };
		    map = new google.maps.Map(document.getElementById("map-canvas2"),mapOptions);
		    calcRoute();
		    directionsDisplay.setMap(map);
		    google.maps.event.trigger(map, "resize");

		});



		google.maps.event.addDomListener(window, 'load', initialize);

		function eliminarVehiculo()
		{
			var con = confirm("¿Desea Eliminar los Vehículos?"); 
			if (con==true)
			{
				alert("si");
			}
		}


	</script>
</body>
</html>