<!DOCTYPE HTML>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Transvega - Contacto</title>
		<link rel="shortcut icon" type="image/x-icon" href="img/transvega-logo.png">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="css/icomoon.css">
		<link rel="stylesheet" type="text/css" href="css/principal.css">
	</head>
	<body>
		<div class="wrapper">
			<div class="container-fluid">
				<div id="nav-bar" class="navbar navbar-default navbar-fixed-top navbar-fijo">
					<div class="container">
						<div class="navbar-header page-scroll">
							<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#menu-principal">
								<span class="sr-only">Toggle Navegación</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="page-scroll" href="index.php"><img class="img-responsive logo" src="img/transvega-logo.png"></a>
						</div>
						
						<div class="collapse navbar-collapse" id="menu-principal">
							<ul class="nav navbar-nav">
								<li class="nombre-header">Transvega</li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
								<li><a href="servicios.php">Servicios</a></li>
								<li><a href="rutas.php">Rutas</a></li>
								<li><a href="nosotros.php">Nosotros</a></li>
								<li><a class="activo" href="#">Contáctenos</a></li>
								<li class="log"><a href="registro.php">Registro</a></li>
								<li class="log"><a href="#modalLogin" data-toggle="modal">Ingreso</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<br>
			<br>
			<br>
			<br>
			<!---------------------------------------------------------------------------FIN MENU PRINCIPAL-------------------------------------------------------------------------------------->
			<div class="container">
				<div class="row modulo-titulo hidden-xs">
					<h1 class="titulo text-center">Contáctenos</h1>
				</div>
				<div class="row modulo-titulo visible-xs">
					<h1 class="titulo-p text-center">Contáctenos</h1>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-lg-6">
						<br>
						<form id="contacto" class="form-horizontal">
							
							<div class="form-group">
								<label class="col-xs-12 col-sm-4 control-label"></label>
								<div class="col-xs-12 col-sm-8">
									<h2 class="text-center"><strong>Formulario de Contacto</strong></h2>
								</div>
							</div>
							<div class="form-group">
						    	<label for="nombre" class="col-xs-12 col-sm-4 control-label">Nombre</label>
						    	<div class="col-xs-12 col-sm-8">
						    		<input type="text" name="nombre" class="form-control" id="nombre" placeholder="" required autofocus>
								</div>
							</div>
							<div class="form-group">
						    	<label for="apellido" class="col-xs-12 col-sm-4 control-label">Apellido</label>
						    	<div class="col-xs-12 col-sm-8">
						    		<input type="text" name="apellido" class="form-control" id="apellido" placeholder="" required>
						    	</div>
							</div>
							<div class="form-group">
						    	<label for="telefono" class="col-xs-12 col-sm-4 control-label">Teléfono</label>
						    	<div class="col-xs-12 col-sm-8">
						    		<input type="tel" name="telefono" class="form-control" id="telefono" placeholder="" required>
						    	</div>
							</div>
							<div class="form-group">
						    	<label for="correo" class="col-xs-12 col-sm-4 control-label">Correo</label>
						    	<div class="col-xs-12 col-sm-8">
						    		<input type="email" name="correo" class="form-control" id="correo" placeholder="" required>
						    	</div>
							</div>
							<div class="form-group">
						    	<label for="mensaje" class="col-xs-12 col-sm-4 control-label">Mensaje</label>
						    	<div class="col-xs-12 col-sm-8">
						    		<textarea name="mensaje" id="mensaje" class="form-control" rows="4"></textarea>
						    	</div>
							</div>
							
							<div class="form-group">
								<div class="col-xs-12 col-sm-6 col-sm-push-6 col-md-4 col-md-push-8 col-lg-4 col-lg-push-8">
									<button type="button" id="BotonEnviar" data-loading-text="Enviando..." class="btn btn-primary btn-block btn-lg" autocomplete="off">
		  								Enviar
									</button>
								</div>
							</div>
		        		</form>
					</div>

					<div class="col-xs-12 col-lg-6">
						<br>
						<h2 class="text-center"><strong>Ubicanos</strong></h2>
						<br>
						<div id="map-canvas" class="map-canvas form-group"></div>
						<br>
							<p class="lead text-right"><strong>Representante: </strong>Abg. Rafael Vega Vitelli</p>
							<p class="lead text-right"><strong>Correo: </strong>informacion@transvega.com.ve</p>
							<p class="lead text-right"><strong>Web: </strong><a href="www.transvega.com.ve"></a>www.transvega.com.ve</p>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<?php
			include'includes/modals-cliente.php';
			include'includes/footer.php';
		?>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>

		<script>
			function initialize() 
			{
			  	var mapOptions = 
			  	{
			    	zoom: 15,
			    	center: new google.maps.LatLng(10.499344, -66.843956)
			  	};

			  	var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

			  	var marker = new google.maps.Marker(
			  	{
			    	position: map.getCenter(),
			    	map: map,
			    	title: 'Ubicanos'
			  	});
			}
			google.maps.event.addDomListener(window, 'load', initialize);

			$(function () 
			{
	  			$('[data-toggle="popover"]').popover();
	  			$('*').scrollTop(0);
			})
			
		</script>
	</body>
</html>