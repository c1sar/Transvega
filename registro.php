<!DOCTYPE HTML>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Transvega - Registro</title>
		<link rel="shortcut icon" type="image/x-icon" href="img/transvega-logo.png">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="css/icomoon.css">
		<link rel="stylesheet" type="text/css" href="css/principal.css">
	</head>
	<body>
		<div class="wrapper">
			<div class="container-fluid">
				<div id="nav-bar" class="navbar navbar-default navbar-fixed-top navbar-fijo">
					<div class="container">
						<div class="navbar-header page-scroll">
							<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#menu-principal">
								<span class="sr-only">Toggle Navegación</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="page-scroll" href="index.php"><img class="img-responsive logo" src="img/transvega-logo.png"></a>
						</div>
						
						<div class="collapse navbar-collapse" id="menu-principal">
							<ul class="nav navbar-nav">
								<li class="nombre-header">Transvega</li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
								<li><a href="servicios.php">Servicios</a></li>
								<li><a href="rutas.php">Rutas</a></li>
								<li><a href="nosotros.php">Nosotros</a></li>
								<li><a href="contacto.php">Contáctenos</a></li>
								<li class="log"><a class="activo" href="#">Registro</a></li>
								<li class="log"><a href="#modalLogin" data-toggle="modal">Ingreso</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<br>
			<br>
			<br>
			<br>
			<!---------------------------------------------------------------------------FIN MENU PRINCIPAL-------------------------------------------------------------------------------------->
			<div class="container">
				<div class="row modulo-titulo hidden-xs">
					<h1 class="titulo text-center">Registro</h1>
				</div>
				<div class="row modulo-titulo visible-xs">
					<h1 class="titulo-p text-center">Registro</h1>
				</div>
				<br>
			</div>
			
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h2 class="text-center"><strong>Formulario de Registro</strong></h2>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
						<div class="row" id="alertas"></div>
						<form id="contacto" class="form-horizontal">
							<div class="form-group">
						    	<label for="nombre" class="col-xs-12 col-sm-2 col-md-4 control-label">Nombre:</label>
						    	<div class="col-xs-12 col-sm-5 col-md-4" id="div-nombre">
						    		<input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" required autofocus>
								</div>
								<div class="col-xs-12 col-sm-5 col-md-4" id="div-apellido">
						    		<input type="text" name="apellido" class="form-control" id="apellido" placeholder="Apellido" required>
								</div>
							</div>
							<div class="form-group">
						    	<label for="estado" class="col-xs-12 col-sm-2 col-md-4 control-label">Estado:</label>
						    	<div class="col-xs-12 col-sm-5 col-md-4" id="div-estado"> 
						    		<select class="form-control" name="estado" id="estado">
										<option value="0">-Seleccione-</option>
										<option>Amazonas</option>
										<option>Anzoategui</option>
										<option>Apure</option>
										<option>Aragua</option>
										<option>Barinas</option>
										<option>Bolivar</option>
										<option>Carabobo</option>
										<option>Cojedes</option>
										<option>Delta Amacuro</option>
										<option selected>Distrito Capital</option>
										<option>Falcón</option>
										<option>Guarico</option>
										<option>Lara</option>
										<option>Mérida</option>
										<option>Miranda</option>
										<option>Monagas</option>
										<option>Nueva Esparta</option>
										<option>Portuguesa</option>
										<option>Sucre</option>
										<option>Táchira</option>
										<option>Trujillo</option>
										<option>Vargas</option>
										<option>Yaracuy</option>
										<option>Zulia</option>
									</select>
								</div>
								<div class="col-xs-12 col-sm-5 col-md-4" id="div-ciudad">
						    		<input type="text" name="ciudad" class="form-control" id="ciudad" placeholder="Ciudad" required>
								</div>
							</div>
							<div class="form-group" id="div-fecha">
						    	<label for="fechaNacimiento" class="col-xs-12 col-sm-2 col-md-4 control-label">Fecha Nacimiento:</label>
						    	<div class="col-xs-4 col-sm-2" id="div-dia">
						    		<select id="fechaNacDia" name="fechaNacDia" class="form-control" required>
										<option value="0" selected="selected">Día</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
										<option value="13">13</option>
										<option value="14">14</option>
										<option value="15">15</option>
										<option value="16">16</option>
										<option value="17">17</option>
										<option value="18">18</option>
										<option value="19">19</option>
										<option value="20">20</option>
										<option value="21">21</option>
										<option value="22">22</option>
										<option value="23">23</option>
										<option value="24">24</option>
										<option value="25">25</option>
										<option value="26">26</option>
										<option value="27">27</option>
										<option value="28">28</option>
										<option value="29">29</option>
										<option value="30">30</option>
										<option value="31">31</option>
									</select>
						    	</div>
						    	<div class="col-xs-4 col-sm-3" id="div-mes">
						    		<select id="fechaNacMes" name="fechaNacMes" class="form-control" required>
										<option value="0" selected="selected">Mes</option>
										<option value="1">Enero</option>
										<option value="2">Febrero</option>
										<option value="3">Marzo</option>
										<option value="4">Abril</option>
										<option value="5">Mayo</option>
										<option value="6">Junio</option>
										<option value="7">Julio</option>
										<option value="8">Agosto</option>
										<option value="9">Septiembre</option>
										<option value="10">Octubre</option>
										<option value="11">Noviembre</option>
										<option value="12">Diciembre</option>
									</select>
						    	</div>
						    	<div class="col-xs-4 col-sm-3" id="div-year">
						    		<select id="fechaNacYear" name="fechaNacYear" class="form-control" required>
										<option value="0" selected="selected">Año</option>
										<option value="2015">2015</option>
										<option value="2014">2014</option>
										<option value="2013">2013</option>
										<option value="2012">2012</option>
										<option value="2011">2011</option>
										<option value="2010">2010</option>
										<option value="2009">2009</option>
										<option value="2008">2008</option>
										<option value="2007">2007</option>
										<option value="2006">2006</option>
										<option value="2005">2005</option>
										<option value="2004">2004</option>
										<option value="2003">2003</option>
										<option value="2002">2002</option>
										<option value="2001">2001</option>
										<option value="2000">2000</option>
										<option value="1999">1999</option>
										<option value="1998">1998</option>
										<option value="1997">1997</option>
										<option value="1996">1996</option>
										<option value="1995">1995</option>
										<option value="1994">1994</option>
										<option value="1993">1993</option>
										<option value="1992">1992</option>
										<option value="1991">1991</option>
										<option value="1990">1990</option>
										<option value="1989">1989</option>
										<option value="1988">1988</option>
										<option value="1987">1987</option>
										<option value="1986">1986</option>
										<option value="1985">1985</option>
										<option value="1984">1984</option>
										<option value="1983">1983</option>
										<option value="1982">1982</option>
										<option value="1981">1981</option>
										<option value="1980">1980</option>
										<option value="1979">1979</option>
										<option value="1978">1978</option>
										<option value="1977">1977</option>
										<option value="1976">1976</option>
										<option value="1975">1975</option>
										<option value="1974">1974</option>
										<option value="1973">1973</option>
										<option value="1972">1972</option>
										<option value="1971">1971</option>
										<option value="1970">1970</option>
										<option value="1969">1969</option>
										<option value="1968">1968</option>
										<option value="1967">1967</option>
										<option value="1966">1966</option>
										<option value="1965">1965</option>
										<option value="1964">1964</option>
										<option value="1963">1963</option>
										<option value="1962">1962</option>
										<option value="1961">1961</option>
										<option value="1960">1960</option>
										<option value="1959">1959</option>
										<option value="1958">1958</option>
										<option value="1957">1957</option>
										<option value="1956">1956</option>
										<option value="1955">1955</option>
										<option value="1954">1954</option>
										<option value="1953">1953</option>
										<option value="1952">1952</option>
										<option value="1951">1951</option>
										<option value="1950">1950</option>
										<option value="1949">1949</option>
										<option value="1948">1948</option>
										<option value="1947">1947</option>
										<option value="1946">1946</option>
										<option value="1945">1945</option>
										<option value="1944">1944</option>
										<option value="1943">1943</option>
										<option value="1942">1942</option>
										<option value="1941">1941</option>
										<option value="1940">1940</option>
										<option value="1939">1939</option>
										<option value="1938">1938</option>
										<option value="1937">1937</option>
										<option value="1936">1936</option>
										<option value="1935">1935</option>
										<option value="1934">1934</option>
										<option value="1933">1933</option>
										<option value="1932">1932</option>
										<option value="1931">1931</option>
										<option value="1930">1930</option>
										<option value="1929">1929</option>
										<option value="1928">1928</option>
										<option value="1927">1927</option>
										<option value="1926">1926</option>
										<option value="1925">1925</option>
										<option value="1924">1924</option>
										<option value="1923">1923</option>
										<option value="1922">1922</option>
										<option value="1921">1921</option>
										<option value="1920">1920</option>
										<option value="1919">1919</option>
										<option value="1918">1918</option>
										<option value="1917">1917</option>
										<option value="1916">1916</option>
										<option value="1915">1915</option>
										<option value="1914">1914</option>
										<option value="1913">1913</option>
										<option value="1912">1912</option>
										<option value="1911">1911</option>
										<option value="1910">1910</option>
										<option value="1909">1909</option>
										<option value="1908">1908</option>
										<option value="1907">1907</option>
										<option value="1906">1906</option>
										<option value="1905">1905</option>
									</select>
						    	</div>
						    	<div class="col-xs-12 col-sm-5">
						    		
						    	</div>
							</div>
							<div class="form-group">
						    	<label for="correo" class="col-xs-12 col-sm-2 col-md-4 control-label">Correo Electrónico:</label>
						    	<div class="col-xs-12 col-sm-5 col-md-4" id="div-correo1">
						    		<input type="email" name="correo" class="form-control" id="correo1" placeholder="Ingresa tu Correo" required>
								</div>
								<div class="col-xs-12 col-sm-5 col-md-4" id="div-correo2">
						    		<input type="email" name="correo" class="form-control" id="correo2" placeholder="Confirmar Correo" required>
								</div>
							</div>
							<div class="form-group">
						    	<label for="contrax" class="col-xs-12 col-sm-2 col-md-4 control-label">Contraseña:</label>
						    	<div class="col-xs-12 col-sm-5 col-md-4" id="div-contrax1">
						    		<input type="password" name="contrax" class="form-control" id="contrax1" placeholder="Ingresa tu Contraseña" required>
								</div>
								<div class="col-xs-12 col-sm-5 col-md-4" id="div-contrax2">
						    		<input type="password" name="contrax" class="form-control" id="contrax2" placeholder="Confirmar Contraseña" required>
								</div>
							</div>
							<div class="form-group">
						    	<label for="telefono" class="col-xs-12 col-sm-2 col-md-4 control-label">Teléfono:</label>
						    	<div class="col-xs-12 col-sm-5 col-md-4" id="div-telefono">
						    		<input type="tel" name="telefono" class="form-control" id="telefono" placeholder="Número telefónico" required>
						    	</div>
							</div>
							<div class="form-group">
								<div class="checkbox col-sm-8 col-sm-push-2 col-md-push-4" id="div-registro">
									<label>
										<input type="checkbox" value="1" id="registro" name=""> Regístrate para recibir noticias y ofertas especiales de Transvega en la dirección de e-mail vinculada a esta cuenta.
									</label>
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12 col-sm-5 col-sm-push-7 col-md-4  col-md-push-8 ">
									<button type="button" id="BotonRegistrar" data-loading-text="Registrando..." class="btn btn-primary btn-block btn-lg" autocomplete="off" onClick="validar();">
		  								Registrar
									</button>
								</div>
							</div>
		        		</form>
					</div>

					<div class="col-xs-12 col-lg-2">
						


					</div>
				</div>
			</div>
		</div>

		</div>		
		<?php
			include'includes/modals-cliente.php';
			include'includes/footer.php';
		?>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
		<script>
			$(function () 
			{
	  			$('[data-toggle="popover"]').popover();
	  			$('*').scrollTop(0);
			})

			function validarEmail(valor) 
			{
				if (/(\w+)(\.?)(\w*)(\@{1})(\w+)(\.?)(\w*)(\.{1})(\w{2,3})/.test(valor))
				{
			    	return true;
			  	} 
			  	else 
			  	{
			    	return false;
			  	}	
			}


			function validar()
			{
				$(".alert" ).remove();
				var vacio=false;
				var error=false;
				var nombre = $('#nombre').val();
				var apellido = $('#apellido').val();
				var estado = $('#estado').val();
				var ciudad = $('#ciudad').val();
				var dia = $('#fechaNacDia').val();
				var mes = $('#fechaNacMes').val();
				var year = $('#fechaNacYear').val();
				var correo1 = $('#correo1').val();
				var correo2 = $('#correo2').val();
				var contrax1 = $('#contrax1').val();
				var contrax2 = $('#contrax2').val();
				var telefono = $('#telefono').val();
				var registro = $('#registro').val();
				var alerta;

				if (nombre=="")
				{
					$("#div-nombre").removeClass("has-success");
					$("#div-nombre").addClass("has-error");
					error=true;
					vacio=true;
				}
				else
				{
					if ((nombre.search(/[0-9]/)== -1)&&(nombre.search(/(\s|\\|\/|!|"|ú|\$|%|&|\(|\)|=|\?|¨|\||@|#|ª|€|\^|`|\[|\]|\+|\*|ù|ï|\{|\}|\-|_|\.|:|,|;|>|<)/ )== -1))
					{
						$("#div-nombre").removeClass("has-error");
						$("#div-nombre").addClass("has-success");
					}
					else
					{
						alerta = '<div class="alert alert-danger col-md-8 col-md-push-4 alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>¡Error!</strong> - Debe Insertar un Nombre Válido </div>';
						$(alerta).appendTo("#alertas");
						$("#div-nombre").removeClass("has-success");
						$("#div-nombre").addClass("has-error");
						error=true;
					}
				}

				if (apellido=="")
				{
					$("#div-apellido").removeClass("has-success");
					$("#div-apellido").addClass("has-error");
					error=true;
					vacio=true;
				}
				else
				{
					if ((apellido.search(/[0-9]/)== -1)&&(apellido.search(/(\s|\\|\/|!|"|ú|\$|%|&|\(|\)|=|\?|¨|\||@|#|ª|€|\^|`|\[|\]|\+|\*|ù|ï|\{|\}|\-|_|\.|:|,|;|>|<)/ )== -1))
					{
						$("#div-apellido").removeClass("has-error");
						$("#div-apellido").addClass("has-success");
					}
					else
					{
						alerta = '<div class="alert alert-danger col-md-8 col-md-push-4 alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>¡Error!</strong> - Debe Insertar un Apellido Válido </div>';
						$(alerta).appendTo("#alertas");
						$("#div-apellido").removeClass("has-success");
						$("#div-apellido").addClass("has-error");
						error=true;
					}
				}
				
				
				//*******************************Validar Correo************************************
				if((!validarEmail(correo1))||(!validarEmail(correo2)))
				{
					alerta = '<div class="alert alert-danger col-md-8 col-md-push-4 alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>¡Error!</strong> - Debe Insertar un Correo Válido </div>';
					$(alerta).appendTo("#alertas");
					if(!validarEmail(correo1))
					{
						$("#div-correo1").removeClass("has-success");
						$("#div-correo1").addClass("has-error");
					}
					if(!validarEmail(correo2))
					{
						$("#div-correo2").removeClass("has-success");
						$("#div-correo2").addClass("has-error");
					}
					error=true;
				}
				else
				{
					if(correo1!=correo2)
					{
						alerta = '<div class="alert alert-danger col-md-8 col-md-push-4 alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>¡Error!</strong> - Los correos no coinciden</div>';
						$(alerta).appendTo("#alertas");
						$("#div-correo1").removeClass("has-success");
						$("#div-correo1").addClass("has-error");
						$("#div-correo2").removeClass("has-success");
						$("#div-correo2").addClass("has-error");
						error=true;
					}
					else
					{
						alerta = '<div class="alert alert-info col-md-8 col-md-push-4 alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>¡Correcto!</strong> - El Correo es Válido </div>';
						$(alerta).appendTo("#alertas");
						$("#div-correo1").removeClass("has-error");
						$("#div-correo1").addClass("has-success");
						$("#div-correo2").removeClass("has-error");
						$("#div-correo2").addClass("has-success");
					}
		
				}
				//*******************************Validar Contraseñas************************************
				if(contrax1!=contrax2)
				{
					alerta = '<div class="alert alert-danger col-md-8 col-md-push-4 alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>¡Error!</strong> - Las contraseñas no coinciden</div>';
					$(alerta).appendTo("#alertas");
					$("#div-contrax1").removeClass("has-success");
					$("#div-contrax1").addClass("has-error");
					$("#div-contrax2").removeClass("has-success");
					$("#div-contrax2").addClass("has-error");
					error=true;
				}
				else
				{
					if(contrax1.length<8)
					{
						alerta = '<div class="alert alert-danger col-md-8 col-md-push-4 alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>¡Error!</strong> - La contraseña es muy corta</div>';
						$(alerta).appendTo("#alertas");
						$("#div-contrax1").removeClass("has-success");
						$("#div-contrax1").addClass("has-error");
						$("#div-contrax2").removeClass("has-success");
						$("#div-contrax2").addClass("has-error");
						error=true;
					}
					else
					{
						if((contrax1.search(/[0-9]/)== -1)||(contrax1.search(/[a-z]/)== -1))
						{
							alerta = '<div class="alert alert-warning col-md-8 col-md-push-4 alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>¡Advertencia!</strong> - La contraseña es poco Segura</div>';
							$(alerta).appendTo("#alertas");
							$("#div-contrax1").removeClass("has-error");
							$("#div-contrax1").addClass("has-success");
							$("#div-contrax2").removeClass("has-error");
							$("#div-contrax2").addClass("has-success");
						}
						else
						{
							alerta = '<div class="alert alert-info col-md-8 col-md-push-4 alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>¡Correcto!</strong> - La contraseña es Segura</div>';
							$(alerta).appendTo("#alertas");
							$("#div-contrax1").removeClass("has-error");
							$("#div-contrax1").addClass("has-success");
							$("#div-contrax2").removeClass("has-error");
							$("#div-contrax2").addClass("has-success");
						}
					}
				}
				
				//*******************************Validar Fechas************************************


				if((dia=="0")||(mes=="0")||(year=="0"))
				{
					error=true;
					alerta = '<div class="alert alert-danger col-md-8 col-md-push-4 alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>¡Error!</strong> - Debe escoger una fecha válida</div>';
					$(alerta).appendTo("#alertas");
				}

				if(dia=="0")
				{
					$("#div-dia").removeClass("has-success");
					$("#div-dia").addClass("has-error");
				}
				else
				{
					$("#div-dia").removeClass("has-error");
					$("#div-dia").addClass("has-success");
				}

				if(mes=="0")
				{
					$("#div-mes").removeClass("has-success");
					$("#div-mes").addClass("has-error");
				}
				else
				{
					$("#div-mes").removeClass("has-error");			
					$("#div-mes").addClass("has-success");
				}

				if(year=="0")
				{
					$("#div-year").removeClass("has-success");
					$("#div-year").addClass("has-error");
				}
				else
				{
					$("#div-year").removeClass("has-error");
					$("#div-year").addClass("has-success");
				}				


				//*******************************Validar Estado************************************

				if (estado=="0")
				{
					error=true;
					alerta = '<div class="alert alert-danger col-md-8 col-md-push-4 alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>¡Error!</strong> - Debe escoger un Estado válido</div>';
					$(alerta).appendTo("#alertas");
					$("#div-estado").removeClass("has-success");
					$("#div-estado").addClass("has-error");
				}
				else
				{
					$("#div-estado").removeClass("has-error");
					$("#div-estado").addClass("has-success");
				}


				if (vacio==true)
				{
					alerta = '<div class="alert alert-danger col-md-8 col-md-push-4 alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>¡Error!</strong> - Debe rellenar los campos obligatorios</div>';
					$(alerta).appendTo("#alertas");
				}


				if(error==false)
				{
					alerta = '<div class="alert alert-info col-md-8 col-md-push-4 alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>¡Correcto!</strong> - Se ha registrado Correctamente, Debe confirmar el correo para poder acceder a su cuenta</div>';
					$(".alert" ).remove();
					$(alerta).appendTo("#alertas");

					$('#nombre').val("");
					$('#apellido').val("");
					$('#estado option[value="0"]').attr("selected", "selected");
					$('#ciudad').val("");
					$('#fechaNacDia option[value="0"]').attr("selected", "selected");
					$('#fechaNacMes option[value="0"]').attr("selected", "selected");
					$('#fechaNacYear option[value="0"]').attr("selected", "selected");
					$('#correo1').val("");
					$('#correo2').val("");
					$('#contrax1').val("");
					$('#contrax2').val("");
					$('#telefono').val("");

					$("#div-nombre").removeClass("has-success");
					$("#div-apellido").removeClass("has-success");
					$("#div-estado").removeClass("has-success");
					$("#div-ciudad").removeClass("has-success");
					$("#div-dia").removeClass("has-success");
					$("#div-mes").removeClass("has-success");
					$("#div-year").removeClass("has-success");
					$("#div-correo1").removeClass("has-success");
					$("#div-correo2").removeClass("has-success");
					$("#div-contrax1").removeClass("has-success");
					$("#div-contrax2").removeClass("has-success");
					$("#div-telefono").removeClass("has-success");
				}
			}

		</script>
	</body>
</html>