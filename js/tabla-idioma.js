$(document).ready(function() {
	$('.table').dataTable({
		"language": 
		{
            "emptyTable":     "No data available in table",
		    "info":           "Mostrando _START_ - _END_ de _TOTAL_ entradas",
		    "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",
		    "infoFiltered":   "(Filtrado de _MAX_ entradas totales)",
		    "infoPostFix":    "",
		    "thousands":      ",",
		    "lengthMenu":     "Mostrar _MENU_ entradas",
		    "loadingRecords": "Cargando...",
		    "processing":     "Procesando...",
		    "search":         "Buscar:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",
		    "zeroRecords":    "No se encuentran resultados",
		    "paginate": 
			    {
			        "first":      "Primero",
			        "last":       "Último",
			        "next":       "Siguiente",
			        "previous":   "Previo"
			    },
		    "aria": 
			    {
			        "sortAscending":  ": activate to sort column ascending",
			        "sortDescending": ": activate to sort column descending"
			    }
		}
	});
});