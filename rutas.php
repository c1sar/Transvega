<!DOCTYPE HTML>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Transvega - Rutas</title>
		<link rel="shortcut icon" type="image/x-icon" href="img/transvega-logo.png">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="css/icomoon.css">
		<link rel="stylesheet" type="text/css" href="css/principal.css">
	</head>
	<body>
		<div class="wrapper">
			<div class="container-fluid">
				<div id="nav-bar" class="navbar navbar-default navbar-fixed-top navbar-fijo">
					<div class="container">
						<div class="navbar-header page-scroll">
							<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#menu-principal">
								<span class="sr-only">Toggle Navegación</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="page-scroll" href="index.php"><img class="img-responsive logo" src="img/transvega-logo.png"></a>
						</div>
						
						<div class="collapse navbar-collapse" id="menu-principal">
							<ul class="nav navbar-nav">
								<li class="nombre-header">Transvega</li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
								<li><a href="servicios.php">Servicios</a></li>
								<li><a class="activo" href="#">Rutas</a></li>
								<li><a href="nosotros.php">Nosotros</a></li>
								<li><a href="contacto.php">Contáctenos</a></li>
								<li class="log"><a href="registro.php">Registro</a></li>
								<li class="log"><a href="#modalLogin" data-toggle="modal">Ingreso</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<br>
			<br>
			<label for="Origen:">Origen:</label>
			<br>
			<br>
			<br>
			<!---------------------------------------------------------------------------FIN MENU PRINCIPAL-------------------------------------------------------------------------------------->
<div class="container">
				<div class="row modulo-titulo hidden-xs">
					<h1 class="titulo text-center">Rutas</h1>
				</div>
				<div class="row modulo-titulo visible-xs">
					<h1 class="titulo-p text-center">Rutas</h1>
				</div>
				<br>
			</div>
			
			<div class="container">
			
				<br>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
						<div class="row" id="alertas"></div>
						<form id="distancias" class="form-horizontal">
						
							<div class="form-group">
						    	<label for="Origen" class="col-xs-12 col-sm-2 col-md-4 control-label">Origen:</label>
						    	<div class="col-xs-12 col-sm-5 col-md-4" id="div-Origen"> 
						    		<select class="form-control" name="origen" id="origen">
										<option value=-1>-Seleccione-</option>
										<option value=0>Acarigua</option>
										<option value=1>Barcelona</option>
										<option value=2>Barinas</option>
										<option value=3>Barquisimeto</option>
										<option value=4>Cabimas</option>
										<option value=5>Caracas</option>
										<option value=6>Carora</option>
										<option value=7>Carúpano</option>
										<option value=8>Ciudad Bolivar</option>
										<option value=9>Pto Ordaz</option>
										<option value=10>Coro</option>
										<option value=11>Cumaná</option>
										<option value=12>El Tigre</option>
										<option value=13>Guanare</option>
										<option value=14>Guasdualito</option>
										<option value=15>La Guaira</option>
										<option value=16>Los Teques</option>
										<option value=17>Maracaibo</option>
										<option value=18>Maracay</option>
										<option value=19>Maturin</option>
										<option value=20>Mérida</option>
										<option value=21>Puerto Cabello</option>
										<option value=22>Puerto La Cruz</option>
										<option value=23>Punto Fijo</option>
										<option value=24>San Antonio Del Tachira</option>
										<option value=25>San Carlos</option>
										<option value=26>San Cristobal</option>
										<option value=27>San Felipe</option>
										<option value=28>San Fdo De Apure</option>
										<option value=29>San Juan De Los Morros</option>
										<option value=30>Trujillo</option>
										<option value=31>Tucupita</option>
										<option value=32>Valencia</option>
										<option value=33>Valera</option>
										<option value=34>Valle De La Pascua</option>

									</select>
								</div>
								
							</div>
							<div class="form-group" id="div-destino">
						    	<label for="destino" class="col-xs-12 col-sm-2 col-md-4 control-label">Destino:</label>
						    	<div class="col-xs-12 col-sm-5 col-md-4" id="div-destino">
						    		<select class="form-control" name="destino" id="destino"   onchange="javascript:calculardist()">
										<option value=-1>-Seleccione-</option>										
										<option value=0>Barcelona</option>
										<option value=1>Barinas</option>
										<option value=2>Barquisimeto</option>	
										<option value=3>Caracas</option>	
										<option value=4>Ciudad Bolivar</option>	
										<option value=5>Pto Ordaz</option>	
										<option value=6>Coro</option>	
										<option value=7>Cumaná</option>	
										<option value=8>Guanare</option>	
										<option value=9>Los Teques</option>	
										<option value=10>Maracaibo</option>	
										<option value=11>Maracay</option>	
										<option value=12>Maturin</option>	
										<option value=13>Mérida</option>	
										<option value=14>San Carlos</option>	
										<option value=15>San Cristobal</option>	
										<option value=16>San Felipe</option>	
										<option value=17>San Fdo De Apure</option>	
										<option value=18>San Juan De Los Morros</option>	
										<option value=19>Trujillo</option>	
										<option value=20>Tucupita</option>	
										<option value=21>Valencia</option>			
									</select>
						    	</div>
						    	
						    	
						    	<div class="col-xs-12 col-sm-5">
						    		
						    	</div>
							</div>
							
					
							
		        		</form>
					</div>

					<div class="col-xs-12 col-lg-2">
						
						

					</div>
				</div>
			</div>
		</div>

		</div>		
		<?php
			include'includes/modals-cliente.php';
			include'includes/footer.php';
		?>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
		<script type="text/javascript">

			function calculardist() {
			
			
			
			var ori = document.getElementById("origen");
			var des = document.getElementById("destino");
			/*
			var origen = $('#origen').val();
			var destino = $('#destino').val();
			
			*/
			var listaOrigen = [
					"Acarigua",
					"Barcelona",
					"Barinas",
					"Barquisimeto",
					"Cabimas",
					"Caracas",
					"Carora",
					"Carúpano",
					"Ciudad Bolivar",
					"Pto Ordaz",
					"Coro",
					"Cumaná",
					"El Tigre",
					"Guanare",
					"Guasdualito",
					"La Guaira",
					"Los Teques",
					"Maracaibo",
					"Maracay",
					"Maturin",
					"Mérida",
					"Puerto Cabello",
					"Puerto La Cruz",
					"Punto Fijo",
					"San Antonio Del Tachira",
					"San Carlos",
					"San Cristobal",
					"San Felipe",
					"San Fdo De Apure",
					"San Juan De Los Morros",
					"Trujillo",
					"Tucupita",
					"Valencia",
					"Valera",
					"Valle De La Pascua"];
			var listaDestino = [
					"Barcelona",
					"Barinas",
					"Barquisimeto",
					"Caracas",	
					"Ciudad Bolivar",	
					"Pto Ordaz",	
					"Coro",	
					"Cumaná",	
					"Guanare",	
					"Los Teques",	
					"Maracaibo",	
					"Maracay",	
					"Maturin",	
					"Mérida",	
					"San Carlos",	
					"San Cristobal",	
					"San Felipe",	
					"San Fdo De Apure",	
					"San Juan De Los Morros",	
					"Trujillo",	
					"Tucupita",	
					"Valencia"
			];		
			
			var tablaDis = [
			[599,	184,	80,	341,	724,	831,	359,	691,	90,	304,	402,	232,	758,	341,	83,	500,	185,	363,	286,	304,	970,	183],
[0 ,	783,	679,	310,	296,	403,	763,	92,	689,	335,	1001,	419,	208,	940,	516,	1093,	587,	519,	395,	929,	420,	468],
[783,	0,	264,	525,	908,	1015,	543,	875,	94,	488,	458,	416,	942,	163,	267,	316,	342,	547,	472,	269,	1154,	367],
[679,	264,	0,	363,	804,	911,	279,	765,	170,	308,	322,	254,	887,	421,	163,	574,	86,	526,	296,	222,	1099,	193],
[972,	404,	299,	622,	1103,	1210,	268,	1064,	460,	607,	32,	553,	1186,	369,	462,	411,	385,	825,	595,	213,	1398,	492],
[310,	525,	363,	0,	591,	698,	453,	402,	431,	25,	706,	109,	518,	682,	258,	841,	277,	400,	139,	585,	730,	158],
[775,	366,	102,	465,	906,	1013,	381,	867,	272,	400,	220,	356,	989,	396,	265,	448,	188,	628,	398,	164,	1201,	307],
[211,	994,	884,	521,	507,	344,	974,	119,	900,	547,	1227,	630,	173,	1151,	720,	1604,	798,	730,	606,	1106,	385,	679],
[296,	908,	804,	591,	0,	107,	963,	388,	814,	616,	1126,	619,	285,	1065,	641,	1218,	787,	689,	565,	1001,	303,	668],
[403,	1015,	911,	698,	107,	0,	1070,	495,	921,	723,	1233,	726,	171,	1172,	748,	1325,	894,	796,	672,	1189,	189,	775],
[763,	543,	279,	453,	963,	1070,	0,	853,	449,	416,	254,	344,	971,	700,	442,	859,	365,	788,	398,	501,	1183,	295],
[92,	875,	765,	402,	388,	495,	853,	0,	781,	427,	1108,	511,	199,	1038,	608,	1191,	680,	616,	492,	1021,	411,	560],
[166,	778,	674,	461,	130,	237,	833,	258,	684,	486,	986,	570,	240,	941,	511,	1094,	685,	540,	435,	865,	447,	520],
[689,	94,	170,	431,	814,	921,	449, 781,	0,	456,	492,	322,	848,	341,	173,	410,	238,	453,	378,	187,	1060,	273],
[1116,	333,	597,	858,	1241,	1348,	876,	1208,	427,	821,	687,	749,	1275,	496,	600,	243,	675,	880,	805,	602,	1487,	700],
[335,	550,	388,	25,	616,	723,	478,	427,	456,	50,	731,	134,	543,	707,	283,	866,	302,	653,	166,	610,	755,	183],
[335,	488,	308,	25,	616,	723,	416,	427,	456,	0,	630,	72,	558,	640,	221,	803,	240,	362,	101,	547,	770,	121],
[1001,	458,	322,	406,	1126,	1233,	254,	1108,	492,	630,	0,	597,	1239,	401,	485,	444,	408,	849,	651,	245,	1239,	549],
[419,	416,	254,	109,	619,	726,	344,	511,	322,	72,	597,	0,	642,	567,	149,	732,	168,	315,	54,	555,	854,	49],
[208,	942,	887,	518,	285,	171,	971,	199,	848,	558,	1239,	642,	0,	1105,	675,	1258,	810,	585,	461,	1035,	212,	676],
[940,	163,	421,	682,	1065,	1172,	700,	1038,	341,	640,	401,	567,	1105,	0,	424,	262,	498,	788,	627,	203,	1317,	525],
[522,	421,	173,	212,	722,	829,	263,	614,	327,	174,	517,	103,	745,	578,	154,	736,	87,	417,	157,	513, 956,	54],
[10,	793,	775,	320,	306,	413,	773,	82,	699,	360,	1011,	429,	218,	887,	526,	1109,	597,	534,	408,	939,	430,	478],
[851,	631,	367,	541,	1051,	1157,	88,	943,	537,	504,	342,	432,	1074,	788,	530,	941,	453,	876,	486,	589,	1271,	383],
[1129,	352,	610,	877,	1254,	1361,	895,	1227,	446,	839, 480,	768,	1294,	298,	619,	36,	696,	983,	822,	406,	1506,	719],
[516,	267,	163,	258,	641,	748,	442,	608,	173,	221,	485,	149,	675,	424,	0,	583,	241,	364,	205,	630,	887,	100],
[1093,	316,	574,	841,	1218,	1325,	859,	1191,	410,	803,	444,	732,	1258,	262,	583,	0,	660,	949,	788,	465,	1470,	683],
[587,	342,	86,	277,	787,	894,	365,	680,	238,	240,	408,	168,	810,	498,	241,	660,	0,	580,	222,	308,	1176,	119],
[519,	547,	526,	400,	689,	796,	788,	616,	453,	362,	849,	315,	585,	788,	364,	949,	580,	0,	261,	667,	797,	364],
[395,	472,	296,	139,	565,	672,	398,	492,	378,	101,	651,	54,	461,	627,	205,	788,	222,	261,	0,	565,	673,	103],
[929,	269,	222,	585,	1001,	1108,	501, 1021,	187,	547,	245,	555,	1035,	203,	360,	465,	308,	667,	565,	0,	1247,	460],
[420,	1154,	1099,	730,	303,	189,	1183,	411,	1060,	770,	1239,	854,	212,	1317,	887,	1470,	1176,	797,	673,	1247,	0,	903],
[468,	367,	193,	158,	668,	775,	295,	560,	273,	121,	549,	49,	676,	525,	100,	683,	119,	364,	103,	460,	903,	0],
[918,	202,	239,	602,	1037,	1144,	512,	1056,	219,	564,	234,	539,	1070,	167,	396,	429,	305,	686,	601,	35,	1283,	409],
[250,	549,	472,	345,	359,	466,	604,	342,	455,	307,	857,	260,	255,	773,	309,	892, 428,	303,	206,	642,	678,	30]

];
			
				/*
			var i =  listaOrigen.indexOf(ori);
			var j =  listaDestino.indexOf(des);
			tablaDistancias[ori][des];
			*/
			
			if (ori.value ==-1){
			alert("Revisa la direccion de origen");}
			else{
			alert("La distancia aproximada es: "+tablaDis[ori.value][des.value]+"Km");
			/*
			var el=document.getElementById( "distancia");
			
			el.value.innerHTML=""+tablaDis[ori.value][des.value].value;
			
			*/
			/*frm.distancia.value = ""+tablaDistancias[i][j];*/
			/*document.getElementById("distancia") = tablaDistancias[i][j];*/
			}
		
			
			}
	</script>
	</body>
</html>