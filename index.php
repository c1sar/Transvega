<!DOCTYPE HTML>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Transvega</title>
	<link rel="shortcut icon" type="image/x-icon" href="img/transvega-logo.png">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="css/icomoon.css">
	<link rel="stylesheet" type="text/css" href="css/principal.css">
</head>
<body>
	<div class="wrapper">
		<div class="container-fluid">
			<div id="nav-bar" class="navbar navbar-default navbar-fixed-top navbar-color1">
				<div class="container-fluid">
					<div class="navbar-header page-scroll">
						<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#menu-principal">
							<span class="sr-only">Toggle Navegación</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="page-scroll" href="index.php"><img class="img-responsive logo" src="img/transvega-logo.png"></a>

					</div>
					<div class="collapse navbar-collapse" id="menu-principal">
						<ul class="nav navbar-nav">
							<li class="nombre-header">Transvega</li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li><a href="servicios.php">Servicios</a></li>
							<li><a href="rutas.php">Rutas</a></li>
							<li><a href="nosotros.php">Nosotros</a></li>
							<li><a href="contacto.php">Contáctenos</a></li>
							<li ><a href="registro.php">Registro</a></li>
							<li ><a href="#modalLogin" data-toggle="modal">Ingreso</a></li>
						</ul>
					</div>
				</div>
			</div>
			
		</div>
		<br>
		<br>
		<br>
		<br>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8">
					<div id="slider" class="carousel slide" data-ride="carousel">
					  <!-- Indicators -->
					 	<ol class="carousel-indicators">
						    <li data-target="#slider" data-slide-to="0" class="active"></li>
						    <li data-target="#slider" data-slide-to="1"></li>
						    <li data-target="#slider" data-slide-to="2"></li>
					  	</ol>

					  <!-- Wrapper for slides -->
					  	<div class="carousel-inner" role="listbox">

					    	<div class="item active">
					      		<img src="img/slider/1.jpg" alt="...">
					      		<div class="carousel-caption">
					        		<h3></h3>
					      		</div>
					    	</div>
						    <div class="item">
						      	<img src="img/slider/2.jpg" alt="...">
						      	<div class="carousel-caption">
						        	<h3></h3>
						      	</div>
						    </div>
					    	<div class="item">
						      	<img src="img/slider/3.jpg" alt="...">
						      	<div class="carousel-caption">
						        	<h3></h3>
						      	</div>
						    </div>
						</div>

					  <!-- Controls -->
						<a class="left carousel-control" href="#slider" role="button" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#slider" role="button" data-slide="next">
						    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						</a>
					</div>
					<br>
				</div>
				<div class="col-xs-12 col-md-4">
					<div class="panel panel-success">
						<div class="panel-heading">
							<h3 class="panel-title text-center">
								<span class="fa fa-truck"></span>
								Rastreo de Carga
							</h3>
						</div>
						<div class="panel-body">
							<form class="form-horizontal" role="form">
								<h4 class="text-justify verde">Por medio de nuestro sistema de rastreo de carga usted podrá localizar y ver el trayecto de su carga hasta llegar a su destino final en Venezuela.</h4>
								<div class="from-group has-feedback">
										<input type="text" class="form-control input-lg" id="rastrear_input" placeholder="Buscar" required autofocus data-container="body" data-toggle="popover" data-placement="top" data-trigger="focus" data-content="Inserte el Código de Rastreo.">
										<span class="form-control-feedback glyphicon glyphicon-search"></span>
								</div>
								<br>
								<button type="button" class="btn btn-success btn-lg btn-block">Buscar</button>

							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!--<header>
			<div class="img-header">
				<div class="container">
					<div class="row row-header">
						<div class="col-xs-12 col-sm-6 col-sm-push-3 col-md-5 col-md-push-2 col-lg-4 col-lg-push-0">
							<div class="panel panel-success">
								<div class="panel-heading">
									<h2 class="panel-title">
										<span class="fa fa-truck"></span>
										Rastreo de Carga
									</h2>
								</div>
								<div class="panel-body">
									<form class="form-horizontal" role="form">
										<div class="from-group has-feedback">
												<input type="text" class="form-control input-lg" id="rastrear_input" placeholder="Buscar" required autofocus data-container="body" data-toggle="popover" data-placement="top" data-trigger="focus" data-content="Inserte el Código de Rastreo.">
												<span class="form-control-feedback glyphicon glyphicon-search"></span>
										</div>
										<br>
										<button type="button" class="btn btn-success btn-lg btn-block">Buscar</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>-->
		
		<seccion id="secciones">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-lg-4 col-md-6 col-sm-6">
						<div class="panel panel-success">
							<div class="panel-heading">
								<h3 class="panel-title text-center">
									<span class="fa fa-wrench"></span>
									<a href="servicios.php">Servicios</a> 
								</h3>
							</div>
							<div class="panel-body item-seccion">
								<img class="img-responsive img-seccion img-center" src="img/servicios.jpg">
						      	<h4 class="text-justify verde">Transvega cuenta con una amplia variedad de servicios, que se ajustan a todas las necesidades.</h4>
						        <button class="btn btn-success btn-lg btn-derecha btn-seccion">Ver más  <i class="fa fa-plus fa-lg"></i></button>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-lg-4 col-md-6 col-sm-6">
						<div class="panel panel-success">
							<div class="panel-heading">
								<h3 class="panel-title text-center">
									<span class="fa fa-random"></span>
									<a href="rutas.php">Rutas</a> 
								</h3>
							</div>
							<div class="panel-body item-seccion">
								<img class="img-responsive img-seccion img-center" src="img/ruta.jpg">
						      	<h4 class="text-justify verde">Nuestra empresa cuenta con una flota de camiones que cubre todo el territorio Nacional.</h4>
						        <button class="btn btn-success btn-lg btn-derecha btn-seccion">Ver más  <i class="fa fa-plus fa-lg"></i></button>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-lg-4 col-md-6 col-sm-6 col-md-push-3 col-sm-push-3 col-lg-push-0">
						<div class="panel panel-success">
							<div class="panel-heading">
								<h3 class="panel-title text-center">
									<span class="fa fa-group"></span>
									Clientes
								</h3>
							</div>
							<div class="panel-body item-seccion">
								<img class="img-responsive img-seccion img-center" src="img/cliente.jpg">
						      	<h4 class="text-justify verde">Contamos con los más distinguidos clientes de Venezuela.</h4>
						        <button class="btn btn-success btn-lg btn-derecha btn-seccion">Ver más  <i class="fa fa-plus fa-lg"></i></button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</seccion>
	</div>

	<br>
	<?php
		include'includes/modals-cliente.php';
		include'includes/footer.php';
	?>

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/header.js"></script>
	<script>
		$(function () 
		{
  			$('[data-toggle="popover"]').popover();
  			$('*').scrollTop(0);
		})

		var tam_ventana = $( window ).width();

		if (tam_ventana>=1000)
		{
			$('#rastrear_input').removeAttr("data-placement");
			$('#rastrear_input').attr("data-placement", "top");
		}
		if (tam_ventana<1000)
		{
			$('#rastrear_input').removeAttr("data-placement");
			$('#rastrear_input').attr("data-placement", "top");
		}

	</script>
</body>
</html>