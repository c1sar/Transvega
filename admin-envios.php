<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Transvega</title>
	<link rel="shortcut icon" type="image/x-icon" href="img/transvega-logo.png">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/principal.css">

</head>

<body>
	<div class="wrapper">
		<div class="container-fluid">
			<div id="nav-bar" class="navbar navbar-default navbar-fixed-top navbar-admin">
				<div class="container">
					<div class="navbar-header page-scroll">
						<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#menu-principal">
							<span class="sr-only">Toggle Navegación</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="page-scroll" href="index.php"><img class="img-responsive logo" src="img/transvega-logo.png"></a>

					</div>
					<div class="collapse navbar-collapse" id="menu-principal">
						<ul class="nav navbar-nav">
							<li class="nombre-header">Transvega</li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li><a class="activo" href="#">Envíos</a></li>
							<li><a href="#">Clientes</a></li>
							<li><a href="#">Empleados</a></li>
							<li><a href="admin-vehiculos.php">Vehículos</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<br>
		<br>
		<br>
		<br>
		<div class="container modulo-titulo"><h2 class="text-center">Gestión Envíos</h2></div>
		<br>
		<div class="container">
			<div class="row">
				<ul class="nav nav-tabs nav-justified submenu">
					<li class="active"><a href="#menu1" data-toggle="tab"><i class="fa fa-bar-chart"></i>&nbsp;&nbsp;Balance</a></li>
					<li><a href="#menu2" data-toggle="tab"><i class="fa fa-truck"></i>&nbsp;&nbsp;Env&iacute;os</a></li>
					<li><a href="#menu3"data-toggle="tab"><i class="fa fa-users"></i>&nbsp;&nbsp;Pagos Personal</a></li>
				</ul>			
				<div class="tab-content tab-submenu">
					<div class="tab-pane active" id="menu1">
						<form id="reporteBalance" class="form-horizontal">
							<br>
							<div class="row">
						    	<div class="form-group" id="div-fecha">
								    <label for="fechaReporteBalance" class="col-xs-12 col-sm-1 col-md-1 control-label">Fecha:</label>
								    <div class="col-xs-4 col-sm-3" id="div-mes">
							    		<select id="fechaNacMes" name="fechaNacMes" class="form-control" required>
											<option value="1" selected="selected">Enero</option>
											<option value="2">Febrero</option>
											<option value="3">Marzo</option>
											<option value="4">Abril</option>
											<option value="5">Mayo</option>
											<option value="6">Junio</option>
											<option value="7">Julio</option>
											<option value="8">Agosto</option>
											<option value="9">Septiembre</option>
											<option value="10">Octubre</option>
											<option value="11">Noviembre</option>
											<option value="12">Diciembre</option>
										</select>
							    	</div>
							    	<div class="col-xs-4 col-sm-3" id="div-year">
							    		<select id="fechaNacYear" name="fechaNacYear" class="form-control" required>
											<option value="2015" selected="selected">2015</option>
											<option value="2014">2014</option>
											<option value="2013">2013</option>
											<option value="2012">2012</option>
										</select>
							    	</div>
							    </div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-7">
									<table id="buscarCamionesT" class="table table-striped table-bordered" cellspacing="0" width="100%">
								
									    <thead>
									        <tr>
									        	<th><span class="fa fa-plus" aria-hidden="true"></span></th>
									            <th class="text-center">Concepto</th>
									            <th class="text-center">Ingreso</th>
									            <th class="text-center">Egreso</th>
									        </tr>
									    </thead>
									 
									    <tfoot>
									        <tr>
									        	<th><span class="fa fa-plus" aria-hidden="true"></span></th>
									            <th>TOTAL:</th>
									            <th class="text-right">245000</th>
									            <th class="text-right">117000</th>
									        </tr>
									    </tfoot>
									 
									    <tbody>
									        <tr>
									        	<td><a href="#CH5400BA" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
									            <td>Envíos</td>
									            <td class="text-right">245000</td>
									            <td></td>
									        </tr>
									        <tr>
									        	<td><a href="#DH5403BA" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
									            <td>Traslados</td>
									            <td class="text-right">0</td>
									            <td></td>
									        </tr>
									        <tr>
									        	<td><a href="#TC7203AB" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
									            <td>Pago Personal - Fijo</td>
									            <td></td>
									            <td class="text-right">36000</td>
									        </tr>
									        <tr>
									        	<td><a href="#F54F3C3" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
									            <td>Pago Personal - Otros</td>
									            <td></td>
									            <td class="text-right">56000</td>
									        </tr>
									        <tr>
									        	<td><a href="#F324C1B9" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
									            <td>Otros Gastos</td>
									            <td></td>
									            <td class="text-right">25000</td>
									        </tr>
									    </tbody>
									    
									</table>

								</div>
								<div class="col-md-5">
									<img class="img-responsive img-thumbnail img-ampliar" src="img/reportes/balance.jpg">
								</div>
							</div>
							<br>
							<div class="col-md-2 col-md-push-10">
								<button class="btn btn-success btn-block"><i class="fa fa-printter"></i>Imprimmir</button>
							</div>
		        		</form>
					</div>
					<div class="tab-pane" id="menu2">
						<br>
						<form id="reporteBalance" class="form-horizontal">
							<div class="row">
						    	<div class="form-group" id="div-fecha">
								    <label for="fechaReporteBalance" class="col-xs-12 col-sm-1 col-md-1 control-label">Fecha:</label>
								    <div class="col-xs-4 col-sm-3" id="div-mes">
							    		<select id="fechaNacMes" name="fechaNacMes" class="form-control" required>
											<option value="1" selected="selected">Enero</option>
											<option value="2">Febrero</option>
											<option value="3">Marzo</option>
											<option value="4">Abril</option>
											<option value="5">Mayo</option>
											<option value="6">Junio</option>
											<option value="7">Julio</option>
											<option value="8">Agosto</option>
											<option value="9">Septiembre</option>
											<option value="10">Octubre</option>
											<option value="11">Noviembre</option>
											<option value="12">Diciembre</option>
										</select>
							    	</div>
							    	<div class="col-xs-4 col-sm-3" id="div-year">
							    		<select id="fechaNacYear" name="fechaNacYear" class="form-control" required>
											<option value="2015" selected="selected">2015</option>
											<option value="2014">2014</option>
											<option value="2013">2013</option>
											<option value="2012">2012</option>
										</select>
							    	</div>
							    </div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-12">
									<table id="reporteBalance" class="table table-striped table-bordered" cellspacing="0" width="100%">
								
									    <thead>
									        <tr>
									        	<th><span class="fa fa-plus" aria-hidden="true"></span></th>
									        	<th class="text-center">Viaje</th>
									            <th class="text-center">Fecha Entrega</th>
									            <th class="text-center">Fecha Carga</th>
									            <th class="text-center">Empresa</th>
									            <th class="text-center">Precio Total</th>
									            <th class="text-center">Observaciones</th>
									        </tr>
									    </thead>
									 
									    <tfoot>
									        <tr>
									        	<th><span class="fa fa-plus" aria-hidden="true"></span></th>
									            <th class="text-center">Viaje</th>
									            <th class="text-center">Fecha Entrega</th>
									            <th class="text-center">Fecha Carga</th>
									            <th class="text-center">Empresa</th>
									            <th class="text-center">Precio Total</th>
									            <th class="text-center">Observaciones</th>
									        </tr>
									    </tfoot>
									 
									    <tbody>
									        <tr>
									        	<td><a href="#CH5400BA" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
									            <td class="text-center">1</td>
									            <td class="text-center">05/01/2015</td>
									            <td class="text-center">04/01/2015</td>
									            <td>Central Madeirense</td>
									            <td class="text-right">22000</td>
									            <td></td>
									        </tr>
									        <tr>
									        	<td><a href="#CH5400BA" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
									            <td class="text-center">2</td>
									            <td class="text-center">07/01/2015</td>
									            <td class="text-center">06/01/2015</td>
									            <td>Plan Suarez</td>
									            <td class="text-right">17000</td>
									            <td></td>
									        </tr>
									        <tr>
									        	<td><a href="#CH5400BA" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
									            <td class="text-center">3</td>
									            <td class="text-center">11/01/2015</td>
									            <td class="text-center">10/01/2015</td>
									            <td>Macro Urbina</td>
									            <td class="text-right">12500</td>
									            <td></td>
									        </tr>
									        <tr>
									        	<td><a href="#CH5400BA" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
									            <td class="text-center">4</td>
									            <td class="text-center">12/01/2015</td>
									            <td class="text-center">11/01/2015</td>
									            <td>Constructora Frankmar</td>
									            <td class="text-right">26500</td>
									            <td></td>
									        </tr>
									        <tr>
									        	<td><a href="#CH5400BA" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
									            <td class="text-center">5</td>
									            <td class="text-center">12/01/2015</td>
									            <td class="text-center">11/01/2015</td>
									            <td>Plan Suarez</td>
									            <td class="text-right">18400</td>
									            <td></td>
									        </tr>
									        <tr>
									        	<td><a href="#CH5400BA" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
									            <td class="text-center">6</td>
									            <td class="text-center">13/01/2015</td>
									            <td class="text-center">12/01/2015</td>
									            <td>Constructora 878</td>
									            <td class="text-right">19200</td>
									            <td></td>
									        </tr>
									        <tr>
									        	<td><a href="#CH5400BA" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
									            <td class="text-center">7</td>
									            <td class="text-center">15/01/2015</td>
									            <td class="text-center">14/01/2015</td>
									            <td>Central Madeirense</td>
									            <td class="text-right">29500</td>
									            <td></td>
									        </tr>
									        <tr>
									        	<td><a href="#CH5400BA" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
									            <td class="text-center">8</td>
									            <td class="text-center">17/01/2015</td>
									            <td class="text-center">16/01/2015</td>
									            <td>Constructora Frankmar</td>
									            <td class="text-right">16500</td>
									            <td></td>
									        </tr>
									        <tr>
									        	<td><a href="#CH5400BA" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
									            <td class="text-center">9</td>
									            <td class="text-center">19/01/2015</td>
									            <td class="text-center">20/01/2015</td>
									            <td>Constructora 878</td>
									            <td class="text-right">19200</td>
									            <td></td>
									        </tr>
									        <tr>
									        	<td><a href="#CH5400BA" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
									            <td class="text-center">10</td>
									            <td class="text-center">19/01/2015</td>
									            <td class="text-center">20/01/2015</td>
									            <td>Pinturas Pinytex, C.A</td>
									            <td class="text-right">15600</td>
									            <td></td>
									        </tr>
									        <tr>
									        	<td><a href="#CH5400BA" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
									            <td class="text-center">11</td>
									            <td class="text-center">20/01/2015</td>
									            <td class="text-center">21/01/2015</td>
									            <td>Festejos y Decoraciones Memorex, C.A </td>
									            <td class="text-right">18500</td>
									            <td></td>
									        </tr>
									        <tr>
									        	<td><a href="#CH5400BA" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
									            <td class="text-center">12</td>
									            <td class="text-center">22/01/2015</td>
									            <td class="text-center">21/01/2015</td>
									            <td>Plan Suarez</td>
									            <td class="text-right">11800</td>
									            <td></td>
									        </tr>
									        <tr>
									        	<td><a href="#CH5400BA" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
									            <td class="text-center">13</td>
									            <td class="text-center">23/01/2015</td>
									            <td class="text-center">22/01/2015</td>
									            <td>Constructora Frankmar</td>
									            <td class="text-right">18300</td>
									            <td></td>
									        </tr>
									    </tbody>
									    
									</table>
								</div>
							</div>
							<br>
							<div class="col-md-2 col-md-push-10">
								<button class="btn btn-success btn-block"><i class="fa fa-printter"></i>Imprimmir</button>
							</div>
						</form>
					</div>

					<div class="tab-pane" id="menu3">
						<div class="tab-content tab-submenu">
					<div class="tab-pane active" id="menu1">
						<form id="reporteBalance" class="form-horizontal">
							<br>
							<div class="row">
						    	<div class="form-group" id="div-fecha">
								    <label for="fechaReporteBalance" class="col-xs-12 col-sm-1 col-md-1 control-label">Fecha:</label>
								    <div class="col-xs-4 col-sm-2" id="div-dia">
							    		<select id="fechaNacDia" name="fechaNacDia" class="form-control" required>
											<option value="0" selected="selected">Día</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
											<option value="6">6</option>
											<option value="7">7</option>
											<option value="8">8</option>
											<option value="9">9</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
											<option value="13">13</option>
											<option value="14">14</option>
											<option value="15">15</option>
											<option value="16">16</option>
											<option value="17">17</option>
											<option value="18">18</option>
											<option value="19">19</option>
											<option value="20">20</option>
											<option value="21">21</option>
											<option value="22">22</option>
											<option value="23">23</option>
											<option value="24">24</option>
											<option value="25">25</option>
											<option value="26">26</option>
											<option value="27">27</option>
											<option value="28">28</option>
											<option value="29">29</option>
											<option value="30">30</option>
											<option value="31">31</option>
										</select>
							    	</div>

								    <div class="col-xs-4 col-sm-3" id="div-mes">
							    		<select id="fechaNacMes" name="fechaNacMes" class="form-control" required>
											<option value="1" selected="selected">Enero</option>
											<option value="2">Febrero</option>
											<option value="3">Marzo</option>
											<option value="4">Abril</option>
											<option value="5">Mayo</option>
											<option value="6">Junio</option>
											<option value="7">Julio</option>
											<option value="8">Agosto</option>
											<option value="9">Septiembre</option>
											<option value="10">Octubre</option>
											<option value="11">Noviembre</option>
											<option value="12">Diciembre</option>
										</select>
							    	</div>
							    	<div class="col-xs-4 col-sm-3" id="div-year">
							    		<select id="fechaNacYear" name="fechaNacYear" class="form-control" required>
											<option value="2015" selected="selected">2015</option>
											<option value="2014">2014</option>
											<option value="2013">2013</option>
											<option value="2012">2012</option>
										</select>
							    	</div>
							    </div>
							    <div class="form-group" id="div-traslado">
							    	<label for="servicio" class="col-md-1 control-label">Servicio:</label>
							    	<div class="col-md-5" id="div-servicio">
							    		<select name="servicio" id="servicio" class="form-control">
							    			<option value="1" select="selected">Central Madeirense 04/01/2015</option>
							    			<option value="2">Plaza Suarez</option>
							    		</select>
							    	</div>
							    	<label for="chofer" class="col-md-1 control-label">Chofer:</label>
							    	<div class="col-md-5" id="div-servicio">
							    		<input name="chofer" id="chofer" type="text" value="José Pérez" disabled class="form-control">
							    	</div>
							    </div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-12">
									<table id="idReportePagos" class="table table-striped table-bordered" cellspacing="0" width="100%">
								
									    <thead>
									        <tr>
									        	<th><span class="fa fa-plus" aria-hidden="true"></span></th>
									            <th class="text-center">ID</th>
									            <th class="text-center">Fecha</th>
									            <th class="text-center">Concepto</th>
									            <th class="text-center">Tipo</th>
									            <th class="text-center">Cantidad</th>
									        </tr>
									    </thead>
									 
									    <tfoot>
									        <tr>
									        	<th><span class="fa fa-plus" aria-hidden="true"></span></th>
									            <th class="text-center">ID</th>
									            <th class="text-center">Fecha</th>
									            <th class="text-center">Concepto</th>
									            <th class="text-center">Tipo</th>
									            <th class="text-center">Abono</th>
									        </tr>
									    </tfoot>
									 
									    <tbody>
									        <tr>
									        	<td><a href="#CH5400BA" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
									            <td class="text-center">1</td>
									            <td class="text-center">04/01/2015</td>
									            <td>Ayudantes</td>
									            <td class="text-center">Efectivo</td>
									            <td class="text-right">2000</td>
									        </tr>
									        <tr>
									        	<td><a href="#CH5400BA" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
									            <td class="text-center">2</td>
									            <td class="text-center">05/01/2015</td>
									            <td>Adelanto</td>
									            <td class="text-center">Cheque 100432</td>
									            <td class="text-right">2300</td>
									        </tr>
											<tr>
									        	<td><a href="#CH5400BA" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
									            <td class="text-center">3</td>
									            <td class="text-center">07/01/2015</td>
									            <td>Adelanto</td>
									            <td class="text-center">Cheque 100435</td>
									            <td class="text-right">1500</td>
									        </tr>
									        <tr>
									        	<td><a href="#CH5400BA" data-toggle="modal"><i class="fa fa-eye"></i></a></td>
									            <td class="text-center">4</td>
									            <td class="text-center">15/01/2015</td>
									            <td>Diferencia</td>
									            <td class="text-center">Cheque 100443</td>
									            <td class="text-right">3200</td>
									        </tr>
									    </tbody>
									    
									</table>

								</div>
							</div>
							<br>
							<div class="col-md-2 col-md-push-10">
								<button class="btn btn-success btn-block"><i class="fa fa-printter"></i>Imprimmir</button>
							</div>
		        		</form>
					</div>

					</div>
				</div>
			</div>
		</div>
		<br>
	</div>
	<footer>
		<div class="container-fluid pie-pagina">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-8 col-lg-8">
						<h4>Transvega C. A. - Rif: J-404377344</h4>
						<p class="small">Telfs: (0212) 3152932, (0416) 6108818, (0426) 5120634</p>
						<p class="small"> Tercera Avenida con Segunda Transversal, Urbanización Los Palos Grandes, Edificio Augustus I Piso 5 Oficina 502, Municipio Chacao, Distrito Capital.</p>
					</div>
					<div class="col-xs-12 col-sm-4 col-lg-4">
						<br>
						<h3 class="text-center">
							<a href="https://www.facebook.com/profile.php?id=100006442804682&fref=ts" target="_blank" class="fa fa-facebook fa-2x social"></a>
							<a href="http://instagram.com/Transvega/" target="_blank" class="fa fa-instagram fa-2x social"></a>
							<a href="https://twitter.com/TransvegaCA" target="_blank" class="fa fa-twitter fa-2x social"></a>
						<h3>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="js/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="js/tabla-idioma.js"></script>
	<script type="text/javascript" src="js/admin.js"></script>
</body>