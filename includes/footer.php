<br>
	<footer>
		<div class="container-fluid pie-pagina">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-8 col-lg-8">
						<h4>Transvega C. A. - Rif: J-404377344</h4>
						<p class="small">Telfs: (0212) 3152932, (0416) 6108818, (0426) 5120634</p>
						<p class="small"> Tercera Avenida con Segunda Transversal, Urbanización Los Palos Grandes, Edificio Augustus I Piso 5 Oficina 502, Municipio Chacao, Distrito Capital.</p>
					</div>
					<div class="col-xs-12 col-sm-4 col-lg-4">
						<br>
						<h3 class="text-center">
							<a href="https://www.facebook.com/profile.php?id=100006442804682&fref=ts" target="_blank" class="fa fa-facebook fa-2x social"></a>
							<a href="http://instagram.com/Transvega/" target="_blank" class="fa fa-instagram fa-2x social"></a>
							<a href="https://twitter.com/TransvegaCA" target="_blank" class="fa fa-twitter fa-2x social"></a>
						<h3>
					</div>
				</div>
			</div>
		</div>
	</footer>