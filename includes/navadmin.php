<div class="container-fluid">
	<div id="nav-bar" class="navbar navbar-default navbar-fixed-top navbar-admin">
		<div class="container">
			<div class="navbar-header page-scroll">
				<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#menu-principal">
					<span class="sr-only">Toggle Navegación</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="page-scroll" href="index.php"><img class="img-responsive logo" src="img/transvega-logo.png"></a>

			</div>
			<div class="collapse navbar-collapse" id="menu-principal">
				<ul class="nav navbar-nav">
					<li class="nombre-header">Transvega</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#">Envíos</a></li>
					<li><a href="#">Clientes</a></li>
					<li><a href="#">Empleados</a></li>
					<li><a href="#">Vehículos</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>