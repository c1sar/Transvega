<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-sm">
	    <div class="modal-content">
	     	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title" id="myModalLabel">Ingreso</h4>
	      	</div>
	      	<div class="modal-body">
				<div class="row">
					<img src="img/transvega-logo.png" class="img-responsive img-center" width="80">
				</div>
				<br>
				<form class="login" action="check.php" method="POST">
					<div class="form-group has-feedback">
						<input type="email" class="form-control" placeholder="Correo" name="email" required autofocus>
						<span class="glyphicon glyphicon-user form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input type="password" class="form-control" placeholder="Contraseña" name="pass" required>
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<button type="submit" class="btn btn-lg btn-primary btn-block">Iniciar Sesión</button>
					<div class="checkbox">
						<label>
							<input type="checkbox" value="1" name=""> No cerrar Sesión	
						</label>
						<p class="help-block"><a href="#">¿Problemas para acceder?</a></p>
					</div>
				</form>
		    </div>
	    </div>
  	</div>
</div>

<div class="social-lateral hidden-xs">
	<ul>
		<li><a href="https://www.facebook.com/profile.php?id=100006442804682&fref=ts" target="_blank" class="icon-facebook"></a></li>
		<li><a href="http://instagram.com/Transvega/" target="_blank" class="icon-instagram"></a></li>
		<li><a href="https://twitter.com/TransvegaCA" target="_blank" class="icon-twitter"></a></li>
		<li><a href="mailto:informacion@transvega.com.ve" target="_blank" class="icon-mail3"></a></li>
	</ul>
</div>