<!DOCTYPE HTML>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Transvega - Nosotros</title>
		<link rel="shortcut icon" type="image/x-icon" href="img/transvega-logo.png">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="css/icomoon.css">
		<link rel="stylesheet" type="text/css" href="css/principal.css">
	</head>
	<body>
		<div class="wrapper">
			<div class="container-fluid">
				<div id="nav-bar" class="navbar navbar-default navbar-fixed-top navbar-fijo">
					<div class="container">
						<div class="navbar-header page-scroll">
							<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#menu-principal">
								<span class="sr-only">Toggle Navegación</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="page-scroll" href="index.php"><img class="img-responsive logo" src="img/transvega-logo.png"></a>
						</div>
						
						<div class="collapse navbar-collapse" id="menu-principal">
							<ul class="nav navbar-nav">
								<li class="nombre-header">Transvega</li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
								<li><a href="servicios.php">Servicios</a></li>
								<li><a href="rutas.php">Rutas</a></li>
								<li><a class="activo" href="#">Nosotros</a></li>
								<li><a href="contacto.php">Contáctenos</a></li>
								<li class="log"><a href="registro.php">Registro</a></li>
								<li class="log"><a href="#modalLogin" data-toggle="modal">Ingreso</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<br>
			<br>
			<br>
			<br>
			<!---------------------------------------------------------------------------FIN MENU PRINCIPAL-------------------------------------------------------------------------------------->
			<div class="container">
				<div class="row modulo-titulo hidden-xs">
					<h1 class="titulo text-center">Conoce más de Nosotros</h1>
				</div>
				<div class="row modulo-titulo visible-xs">
					<h1 class="titulo-p text-center">Nosotros</h1>
				</div>
				<br>
				<div class="row">
					<ul class="nav nav-tabs nav-justified submenu">
						<li class="active"><a href="#menu1" data-toggle="tab"></i>&nbsp;¿Quiénes Somos?</a></li>
						<li><a href="#menu2" data-toggle="tab">&nbsp;Misión</a></li>
						<li><a href="#menu3" data-toggle="tab">&nbsp;Visión</a></li>
						<li><a href="#menu4" data-toggle="tab"></i>&nbsp;Valores</a></li>
					</ul>
					<div class="tab-content tab-submenu">
						<div class="tab-pane active" id="menu1"><!Quiénes Somos>
							<div class="visible-lg col-lg-4 img-center">
								<br>
								<br>
								<br>
								<br>
								<img src="img/nosotros/camion.png" class="img-responsive img-center img-thumbnail">
							</div>
							<div class="visible-sm visible-md col-sm-12">
								<br>
								<br>
								<div class="row"><img src="img/nosotros/camion.png" class="img-responsive img-thumbnail img-center"></div>
							</div>
							<div class="col-xs-12 col-sm-12 col-lg-8">
								<br>
								<h1 class="text-right">¿Quiénes Somos?</h1>
								<br>
								<br>
								<p class="lead text-justify">&nbsp;&nbsp;&nbsp;&nbsp;Transvega, C.A es una empresa dedicada a la prestación de servicio de transporte terrestre, de carga liviana y pesada y/o personas dentro del territorio nacional, tanto para empresas del sector público como del sector privado, incluyendo mercancías variadas, secas y perecederas, carga refrigerada, alimentos, mudanzas, viajes expresos y servicios de encomienda, así como el almacenamiento, distribución, transporte y comercialización de productos. Todo ello en vehículos propios, arrendados y/o afiliados, en el caso de la intermediación de fletes y otras relacionadas con el ramo.</p>
							</div>
							<div class="visible-xs col-xs-12 img-center">
								<img src="img/nosotros/camion.png" class="img-responsive img-center img-thumbnail">
							</div>
						</div>
						<div class="tab-pane" id="menu2"><!Misión>
							<div class="visible-lg col-lg-4 img-center">
								<br>
								<br>
								<br>
								<img src="img/nosotros/mision.png" class="img-responsive img-center">
							</div>
							<div class="visible-sm visible-md col-sm-12">
								<br>
								<br>
								<div class="row"><img src="img/nosotros/mision.png" class="img-responsive img-center"></div>
							</div>
							<div class="col-xs-12 col-sm-12 col-lg-8">
								<br>
								<h1 class="text-right">Misión</h1>
								<br>
								<br>
								<p class="lead text-justify">&nbsp;&nbsp;&nbsp;&nbsp;Convertirnos en aliados estratégicos de nuestros clientes en el transporte terrestre de mercancía a través del nuevo modelo de flota especialmente acondicionada para este fin, generando resultados ganar - ganar para el cliente y la empresa.</p>
							</div>
							<div class="visible-xs col-xs-12 img-center">
								<img src="img/nosotros/mision.png" class="img-responsive img-center">
							</div>
						</div>
						<div class="tab-pane" id="menu3"><!Visión>
							<div class="col-xs-12 col-sm-12 col-lg-8">
								<br>
								<h1 class="text-right">Visión</h1>
								<br>
								<br>
								<p class="lead text-justify">&nbsp;&nbsp;&nbsp;&nbsp;Consolidarnos como empresa líder en el ramo, brindando cada día un mejor servicio, de manera eficiente. Estableciendo alianzas estratégicas comprometidas con las necesidades del cliente y en la búsqueda de un servicio cada vez más integral que nos permita alcanzar un reconocido prestigio nacional. Así como ir de la mano con el dinamismo del mundo actual y las nuevas tendencias de negocio.</p>
							</div>
							<div class="visible-lg col-lg-4 img-center">
								<br>
								<br>
								<br>
								<img src="img/nosotros/vision.png" class="img-responsive img-center">
							</div>
							<div class="visible-sm visible-md col-sm-12">
								<br>
								<br>
								<div class="row"><img src="img/nosotros/vision.png" class="img-responsive img-center"></div>
							</div>
							
							<div class="visible-xs col-xs-12 img-center">
								<img src="img/nosotros/vision.png" class="img-responsive img-center">
							</div>
						</div>
						<div class="tab-pane" id="menu4"><!Valores>
							<div class="visible-lg col-lg-6 img-center">
								<br>
								<br>
								<br>
								<img src="img/nosotros/valores.jpg" class="img-responsive img-center thumbnail">
							</div>
							<div class="visible-sm visible-md col-sm-12">
								<br>
								<br>
								<div class="row"><img src="img/nosotros/valores.jpg" class="img-responsive img-center thumbnail"></div>
							</div>
							<div class="col-xs-12 col-sm-12 col-lg-6">
								<br>
								<h1 class="text-right">Valores</h1>
								<br>
								<br>
								<ul>
									<li><p class="lead">Claridad de Acción<p></li>
									<li><p class="lead">Seguridad<p></li>
									<li><p class="lead">Seriedad y Profesionalismo<p></li>
									<li><p class="lead">Capacidad de Ejecución<p></li>
									<li><p class="lead">Innovación<p></li>
									<li><p class="lead">Responsabilidad y Puntualidad<p></li>
								</ul>
							</div>
							<div class="visible-xs col-xs-12 img-center">
								<img src="img/nosotros/valores.jpg" class="img-responsive img-center thumbnail">
							</div>
						</div>
					</div>
				</div>
			</div>



		</div>

		<?php
			include'includes/modals-cliente.php';
			include'includes/footer.php';
		?>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
		<script>
			$(function () 
			{
	  			$('[data-toggle="popover"]').popover();
	  			$('*').scrollTop(0);
			})
		</script>
	</body>
</html>